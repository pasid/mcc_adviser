package org.modcs.mccadv.spn;

import java.util.ArrayList;

import org.modcs.mccadv.utils.Constants;

/**
 * Basic element that represents a method call.
 */

public class Method{
	private String name;
	private ArrayList<Method> dependsOn; //Lista de m�todos dos quais este depende para ser executado.
	private ArrayList<Method> dependents; //Lista de m�todos que dependem da sa�da deste.
	private double time; //Tempo da chamada
	private double energy; //energy da chamada

	public Method(){
		this.name = "";
		this.dependsOn = new ArrayList<Method>();
		this.dependents = new ArrayList<Method>();
		this.time = 0;
	}

	/**
	 * Get the method call name.
	 * @return Method call name. 
	 */
	public String getName() {
		return name;
	}
	/**
	 * Set the method call name.
	 * @param name Method call name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Method> getDependsOn() {
		return dependsOn;
	}

	public void setDependsOn(Method dependsOn) {
		this.dependsOn.add(dependsOn);
	}

	public ArrayList<Method> getDependents() {
		return dependents;
	}

	public void setDependents(Method dependents) {
		this.dependents.add(dependents);
	}
	/**
	 * Get the method call delay.
	 * @return The method call delay.
	 */
	public double getTime() {
		return time;
	}
	/**
	 * Set the method call delay.
	 * @param time Method call delay.
	 */
	public void setTime(double time) {
		this.time = time;
	}

	public double getEnergy() {
		return energy;
	}

	public void setEnergy(double energy) {
		this.energy = energy;
	}

	public String getNameOfIntermediaryPlace(){
		return Constants.INICIAL_ACRON_SECOND_PLACE + this.getName();
	}

}
