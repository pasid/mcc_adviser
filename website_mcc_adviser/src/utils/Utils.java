package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Utils {

	/** Read the inputTimeFile.txt file.
	 * 
	 * @return The file content.
	 * @throws FileNotFoundException
	 */
	public static String readTimeFile(String fileName) throws FileNotFoundException{
	
		File file = new File(fileName);
		StringBuilder fileContents = new StringBuilder((int)file.length());
		Scanner scanner = new Scanner(file);
		String lineSeparator = System.getProperty("line.separator");
	
		try {
			while(scanner.hasNextLine()) {        
				fileContents.append(scanner.nextLine() + lineSeparator);
			}
	
		} finally {
			scanner.close();
		}
	
		return fileContents.toString();
	}

}
