package controller;



import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
 
@WebListener
public class FileLocationContextListener implements ServletContextListener {
 
    @Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
        String rootPath = System.getProperty("user.dir");
        ServletContext ctx = servletContextEvent.getServletContext();
        //String relativePath = ctx.getInitParameter("tempfile.dir");
        File file = new File(rootPath + File.separator);
        if(!file.exists()) file.mkdirs();
        System.out.println("File Directory created to be used for storing files");
        System.out.println(file.getAbsolutePath());
        ctx.setAttribute("FILES_DIR_FILE", file);
        ctx.setAttribute("FILES_DIR", rootPath + File.separator);
    }
 
    @Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
        //do cleanup if needed
    }
     
}