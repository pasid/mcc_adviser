package br.ufpe.cin.modcs.mccadviser.testbed.mccclient.instrumenter;

public interface Measurable
{
    void registerStart();

    double registerFinishing();
}