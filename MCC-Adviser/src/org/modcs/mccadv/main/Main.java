package org.modcs.mccadv.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.modcs.mccadv.input.FileCreator;
import org.modcs.mccadv.input.SimulationManagerFixedConfig;
import org.modcs.mccadv.spn.AnalysisPerformer;
import org.modcs.mccadv.utils.Constants;
import org.modcs.mccadv.utils.MetricEnum;
import org.modcs.mccadv.utils.PropertiesReader;


public class Main {

	public static void main(String[] args) throws Exception {

		if (PropertiesReader.FIXED_CONFIGURATION){
				new SimulationManagerFixedConfig().run();
		} else {
			
			//These two activities must be done one first, later the other.
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Choose between the metrics: \"time\" x \"energy\":");
			String input = br.readLine();
			System.out.println();
			
			while (!input.equals("time") && !input.equals("energy")) {
				System.out.println("Choose between the metrics: \"time\" x \"energy\":");
				input = br.readLine();
				System.out.println();
			}
			
			System.out.println("Do you want to create the file(s) to insert input parameters? (yes/no)");
			String createFileToInputParameters = br.readLine();
			System.out.println();
			
			while (!createFileToInputParameters.equals("yes") && !createFileToInputParameters.equals("no")) {
				System.out.println("Do you want we create the file(s) to insert input parameters? (yes/no)");
				createFileToInputParameters = br.readLine();
				System.out.println();
			}
			
			
			//Creating the input time file
			if (createFileToInputParameters.equals("yes")) {
				if (input.equals("energy")) {
					new FileCreator(0, Constants.INPUT_ENERGY_FILE_NAME, MetricEnum.ENERGY).createMethodsAndWriteOnFile();
					new FileCreator(0, Constants.INPUT_TIME_FILE_NAME, MetricEnum.ENERGY).createMethodsAndWriteOnFile();
				}else if (input.equals("time")) {
					new FileCreator(0, Constants.INPUT_TIME_FILE_NAME, MetricEnum.TIME).createMethodsAndWriteOnFile();
				}else{
					System.out.println("Invalid option.");
				}
			}
			
			
			if (input.equals("energy")) {
				System.out.println("Did you fill the files inputEnergyFile.txt and inputTimeFile.txt? (yes/no)");
				String fillTheFile = br.readLine();
				System.out.println();
				while(!fillTheFile.equals("yes")){
					System.out.println("Did you fill the files inputEnergyFile.txt and inputTimeFile.txt? (yes/no)");
					fillTheFile = br.readLine();
					System.out.println();
				}
			}else if (input.equals("time")) {
				System.out.println("Did you fill the file inputTimeFile.txt? (yes/no)");
				String fillTheFile = br.readLine();
				System.out.println();
				
				while(!fillTheFile.equals("yes")){
					System.out.println("Did you fill the file inputTimeFile.txt? (yes/no)");
					fillTheFile = br.readLine();
					System.out.println();
				}
			}
			
			//Run the analysis
//			if (input.equals("energy")) {
//				AnalysisPerformer.runFullAnalysis(MetricEnum.ENERGY);
//			}else{
//				AnalysisPerformer.runFullAnalysis(MetricEnum.TIME);
//			}
			
			System.err.println("The evaluation has finished!");
			
		}
	}

	
}
