package instrumenter;

public class InstrumenterUtils {

	public static double initTime = 0;
	ManageFile m = new ManageFile ("Time.txt");
	
	public static void initCapturingTime(){
		initTime = System.currentTimeMillis();
	}

	public static void finishCapturingTime(String methodCallName, int line){
		m.WriteFile(methodCallName+"["+line+"]:"+(System.currentTimeMillis() - initTime));
	}
	
	
}
