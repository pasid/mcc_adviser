package controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class ReadFile {
	public static String readTextFile(String fileName) throws IOException {
		String text = new String(Files.readAllBytes(Paths.get(fileName)));
	    return text;
	}
	
	public static void writeToTextFile(String fileName, String content) throws IOException {
		Files.write(Paths.get(fileName), content.getBytes(), StandardOpenOption.CREATE);
	}
}
