package instrumenter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.xml.bind.ValidationException;

import org.apache.commons.io.FileUtils;

public class Utils {

	public static void main(String[] args) {
		changeLine("C:/Users/Junior/Desktop/Git/mcc_adviser/TargetProject/src/target_package/", "Teste.java",
				"InstrumenterUtils.java");
	}

	public static String searchFile(String dir) throws ValidationException { // Take the file's name from
													// diretory
		File diretory = new File(dir);
		File[] files = diretory.listFiles();
		
		if(files == null){
			throw new ValidationException("The origin dir was not found");		}
		if (files.length == 0) {
			throw new ValidationException("The file to be instrumented was not found");
		}
		
		for (File file : files) {
			if(file.getName().contains("java")){
				return file.getName();	
			}
		}
		return null;
	}

	public static void deleteFilesOnDir(String dir) { // Delete all files on
				
		File diretory = new File(dir);
		
		diretory.listFiles();;
		
		if (diretory.isDirectory()) {
			File[] files = diretory.listFiles();;

			for (File file : files) {
				if (!file.delete()) {
					System.out.println("File "+ file.getName() + " not deleted.");
				}
			}
		}
		  
	}

	
	public static void deleteSpecificFile(String dir, String filename) { 
		File file = new File(dir + filename);
		file.delete();
	}


	/**
	 * muda a primeira linha do arquivo colocando o nome do pacote correto.
	 * @param dir
	 * @param className
	 * @param fileName
	 */
	public static void changeLine(String dir, String className, String fileName) {

		try {
			String classNam = dir + className;
			FileReader fileA = new FileReader(classNam);
			BufferedReader rFileA = new BufferedReader(fileA);
			String firstLine = rFileA.readLine();// Take the first line from
													// "className"
			fileA.close();
			String fileNam = dir + fileName;
			FileReader fileB = new FileReader(fileNam);
			BufferedReader rFileB = new BufferedReader(fileB);
			String line = rFileB.readLine();// Read the first line
			int counterA = 0; // Create the counter for take the amount of the
								// number of lines from fileName
			while (line != null) {
				counterA = counterA + 1;
				line = rFileB.readLine();
			}
			fileB.close();
			FileReader fileC = new FileReader(fileNam);
			BufferedReader rFileC = new BufferedReader(fileC);
			String[] lines = new String[counterA];
			int counterB = 0;
			while (counterB < counterA) {
				lines[counterB] = rFileC.readLine(); // Read and save all of
														// fileName in a array
														// variable
				counterB = counterB + 1;
			}
			fileC.close();
			FileWriter fileD = new FileWriter(fileNam);
			PrintWriter wFileD = new PrintWriter(fileD);
			int counterC = 0;
			lines[0] = firstLine;// The first line from fileName will receive
									// the first line from className
			int len = lines.length;
			while (counterC < len) {
				wFileD.printf("%s\n", lines[counterC]);// Write all from lines[]
														// in the fileName
				counterC = counterC + 1;
			}
			fileD.close();
			wFileD.close();
		} catch (IOException e) {
			System.err.printf("Error for open the file: %s.\n", e.getMessage());
		}
	}

	public static void changeFirstLine(String dir, String fileName) {
		// This method will change the first line in a file "fileName"
		try {
			String diretory = dir;
			String diretorySplitted[] = diretory.split("/");
			String fullPath = dir + fileName; // Take the File's Path
			FileReader fileA = new FileReader(fullPath);
			BufferedReader rFileA = new BufferedReader(fileA);
			String lineA;
			lineA = rFileA.readLine();
			// Read the first line
			int counterA = 0; // Create the counter for take the amount of the
								// number of lines
			while (lineA != null) { // The string "lineA" receives "null" when
									// the loop process reach the last line of
									// the file
				counterA = counterA + 1;
				lineA = rFileA.readLine(); // l� da segunda at� a �ltima linha
			}
			fileA.close();
			// Now send the file's line for a Array
			FileReader fileB = new FileReader(fullPath);
			BufferedReader rFileB = new BufferedReader(fileB);
			String lineB = rFileB.readLine();
			String[] lines = new String[counterA];
			int counterB = 0;
			while (lineB != null) { // The array 0 receive the line 1
				lines[counterB] = lineB; // The array "counter1" receive the
											// line corresponding to the counter
				counterB = counterB + 1;
				lineB = rFileB.readLine();
			}
			fileB.close();
			// Now overwrite the file with the first modified line
			FileWriter file3 = new FileWriter(fullPath);
			PrintWriter wFile = new PrintWriter(file3);
			int counterC = 0; // Counter for write in the file
			int len = lines.length;
			while (counterC < len) {
				if (counterC == 0) { // Here change the first line
					int lenDiretory = diretorySplitted.length;
					wFile.printf("package %s;\n", diretorySplitted[lenDiretory - 1]);
					counterC = counterC + 1;
				} else { // Writes the rest of lines
					wFile.printf("%s\n", lines[counterC]);
					counterC = counterC + 1;
				}
			}
			file3.close();
		} catch (IOException e) {
			System.err.printf("Error for open the file: %s.\n", e.getMessage());
		}
	}
}