# MCC-Adviser 


MCC-Adviser is a tool based on the Mercury engine (https://sites.google.com/site/mercurytooldownload/) that can assist software engineers planning mobile cloud infrastructure. First, MCC-Adviser analyzes the source code, generates Stochastic Petri Nets and then predicts the throughput and tracing probability distribution functions for different number of resources. Every code has a parallelism limit due to its coupling level. This way, it is possible to decide if it is necessary to use more or less resources observing the results.

# Project Features

MCC-Adviser can be used in two manners. In the first (Ad hoc Code), the user can insert its own 
source code to be analysed and in the second (Generated Code) way the tool generates the source code
based on some parameters. In both cases MCC-Adviser generates Stochastic Petri Nets (SPN)
and provide some analysis about it.

<br>
<b>Ad hoc Code Functionality</b><br>
The user insert a class code (named input_methods.java) composed by one method and then 
MCC-Adviser performs the following steps:<br>
<br>
- Analyses the granularity of code. <br>
- In a file named inputTimeFile.txt the user can inform the delays for each method-call. <br>
- The tool Generates an SPN <br>
- A bar plot with throughput values and a line plot (cumulative distribution function) are provided. <br>

<br>

<b>Generated Code Functionality</b><br>
The same steps of the other process are performed but in this case, the  SPN follows a pattern in 
which all process are parallel and the user must inform only: the average delay to the parallel transitions,
the delay for the last transition and the quantity to parallel processes.<br>
- The generated files resides in the folder called "generated_files"<br>
- All parameters can be altered in the configuration file called "config.properties"<br>
- In the config.properties one one very important property is FIXED_CONFIGURATION. If its value is true
the tool will behaviour according to the "Ad hoc Code" functionality, and Generated Code, instead.


Here you can access a first prototype:
http://cin.ufpe.br/~faps/mcc-adv
