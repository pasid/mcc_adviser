package br.ufpe.cin.modcs.mccadviser.testbed.mccclient;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.Semaphore;

import br.ufpe.cin.modcs.mccadviser.testbed.MethodOffloadable;
import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.configuration.Server;
import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.exception.MCCException;
import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.instrumenter.Measurement;

public class MccClientSync
{
    private Server server;
    private Socket socket;

    private BufferedReader in;
    private PrintWriter out;

    private String response = "";

    boolean keepConnection;

    MccClientSync(Server server, boolean keepConnection, boolean autoConnect)
    {
        this.server = server;
        this.keepConnection = keepConnection;

        if (autoConnect)
            connect();
    }

    public Measurement executeRemoteMethod2(final Measurement measurement) throws MCCException
    {
        final Semaphore s = new Semaphore(0);

        Thread t = new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    //if (!isConnected())
                    //connect();

                    measurement.registerStart();

                    //out.println("EXEC#" + measurement.getMethod().getName());
                    out.println("EXEC#methodoffload");
                    out.flush();

                    MethodOffloadable moffload = new MethodOffloadable(measurement.getMethod().getName());

                    try
                    {
                        ObjectOutputStream objectOutput = new ObjectOutputStream(socket.getOutputStream());
                        objectOutput.writeObject(moffload);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                    while(true)
                    {
                        response = in.readLine();

                        if (response != null)
                        {
                            if ((response.contains("offload_return")))
                            {
                                try
                                {
                                    ObjectInputStream objectInput = new ObjectInputStream(socket.getInputStream());

                                    try
                                    {
                                        Object r = objectInput.readObject();

                                        if ((r != null) && (r instanceof MethodOffloadable))
                                        {
                                            moffload = (MethodOffloadable) r;
                                            break;
                                        }
                                    }
                                    catch (ClassNotFoundException e)
                                    {
                                        e.printStackTrace();
                                    }
                                }
                                catch (IOException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    measurement.setMethodOffloadable(moffload);
                    measurement.registerFinishing(response);

                    if (!keepConnection)
                    {
                        out.println("EXEC#close");
                        out.flush();
                    }

                    /*
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
                    byte[] buffer = new byte[1024];

                    int bytesRead;
                    InputStream inputStream = socket.getInputStream();

                    while ((bytesRead = inputStream.read(buffer)) != -1)
                    {
                        byteArrayOutputStream.write(buffer, 0, bytesRead);
                        response += byteArrayOutputStream.toString("UTF-8");

                        Log.d(App.TAG, "response: " + response);
                        System.out.println("response: " + response);
                    }
                    */
                }
                catch (UnknownHostException e)
                {
                    e.printStackTrace();
                    throw new MCCException("The server is unreachable");
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    throw new MCCException("Communication error with the server");
                }
                finally
                {
                    if ((socket != null) && (!keepConnection))
                    {
                        try
                        {
                            in.close();
                            out.close();

                            socket.close();
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }

                s.release();
            }
        };
        t.start();

        try
        {
            s.acquire();
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }

        Log.d(App.TAG, "Retornando o valor de measurement");

        if (measurement == null)
            Log.d(App.TAG, "Measurement is null");
        else
            Log.d(App.TAG, "Measurement isn't null");

        return measurement;
    }


    /********************/

    /*public Measurement executeRemoteMethod2(final Method method,  final int iteration, final int methodIndex, final Context contex) throws MCCException
    {
        final Semaphore s = new Semaphore(0);

        measurement = new Measurement();

        Thread t = new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    //if (!isConnected())
                    //connect();

                    measurement.registerStart();

                    out.println("EXEC#"+method.getName());
                    out.flush();

                    while(true)
                    {
                        response = in.readLine();

                        if (response != null)
                        {
                            int indexReturnMethod = response.indexOf("OK#") + 3;

                            if ((response.contains("ACK#WELCOME")) || (indexReturnMethod < 0))
                                continue;
                            else
                            {
                                response = response.substring(indexReturnMethod);
                                break;
                            }
                        }
                    }

                    measurement.registerFinishing(iteration, methodIndex, method.getTarget(), method.getName(), response, contex);

                    ///Log.d(App.TAG, "response: " + response);

                    if (!keepConnection)
                    {
                        out.println("EXEC#close");
                        out.flush();
                    }

                    /*
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
                    byte[] buffer = new byte[1024];

                    int bytesRead;
                    InputStream inputStream = socket.getInputStream();

                    while ((bytesRead = inputStream.read(buffer)) != -1)
                    {
                        byteArrayOutputStream.write(buffer, 0, bytesRead);
                        response += byteArrayOutputStream.toString("UTF-8");

                        Log.d(App.TAG, "response: " + response);
                        System.out.println("response: " + response);
                    }
                    *
                }
                catch (UnknownHostException e)
                {
                    e.printStackTrace();
                    throw new MCCException("The server is unreachable");
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    throw new MCCException("Communication error with the server");
                }
                finally
                {
                    if ((socket != null) && (!keepConnection))
                    {
                        try
                        {
                            in.close();
                            out.close();

                            socket.close();
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }

                s.release();
            }
        };
        t.start();

        try
        {
            s.acquire();
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }

        Log.d(App.TAG, "Retornando o valor de measurement");

        if (measurement == null)
            Log.d(App.TAG, "Measurement is null");
        else
            Log.d(App.TAG, "Measurement isn't null");

        return measurement;
    } */

    /********************/

    public Object executeRemoteMethod(final String methodName) throws MCCException
    {
        final Semaphore s = new Semaphore(0);

        Thread t = new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    //if (!isConnected())
                        //connect();

                    out.println("EXEC#" + methodName);
                    out.flush();

                    while(true)
                    {
                        response = in.readLine();

                        if (response != null)
                        {
                            int indexReturnMethod = response.indexOf("OK#") + 3;

                            if ((response.contains("ACK#WELCOME")) || (indexReturnMethod < 0))
                                continue;
                            else
                            {
                                response = response.substring(indexReturnMethod);
                                break;
                            }
                        }
                    }

                    ///Log.d(App.TAG, "response: " + response);

                    if (!keepConnection)
                    {
                        out.println("EXEC#close");
                        out.flush();
                    }

                    /*
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
                    byte[] buffer = new byte[1024];

                    int bytesRead;
                    InputStream inputStream = socket.getInputStream();

                    while ((bytesRead = inputStream.read(buffer)) != -1)
                    {
                        byteArrayOutputStream.write(buffer, 0, bytesRead);
                        response += byteArrayOutputStream.toString("UTF-8");

                        Log.d(App.TAG, "response: " + response);
                        System.out.println("response: " + response);
                    }
                    */
                }
                catch (UnknownHostException e)
                {
                    e.printStackTrace();
                    throw new MCCException("The server is unreachable");
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    throw new MCCException("Communication error with the server");
                }
                finally
                {
                    if ((socket != null) && (!keepConnection))
                    {
                        try
                        {
                            in.close();
                            out.close();

                            socket.close();
                        }
                        catch (IOException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }

                s.release();
            }
        };
        t.start();

        try
        {
            s.acquire();
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }

/*        if (error)
        {
            //Log.e(App.TAG, response);
            throw new MCCException(response);
        }*/

        return response;
    }

    private boolean isConnected()
    {
        if (socket == null)
            return false;

        if (socket.isConnected())
            return true;

        return false;
    }

    private void connect() throws MCCException
    {
        try
        {
            socket = new Socket(server.getAddress(), server.getPort());

            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
        }
        catch (UnknownHostException e)
        {
            e.printStackTrace();
            throw new MCCException("The server is unreachable");
        }
        catch (IOException e)
        {
            e.printStackTrace();
            throw new MCCException("Communication error with the serve on connection");
        }
    }

    public  void disconnect() throws MCCException
    {
        if (socket != null)
        {
            try
            {
                if (socket.isConnected())
                {
                    out.println("EXEC#close");
                    out.flush();

                    in.close();
                    out.close();

                    socket.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
                throw new MCCException("Error to close connection");
            }
            finally
            {
                socket = null;
                in = null;
                out = null;
            }
        }
    }
}