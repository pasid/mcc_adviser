package controller;

import java.io.IOException;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utils.Utils;

/**
 * Servlet implementation class ReadFileInstServlet
 */
@WebServlet("/ReadFileInstServlet")
public class ReadFileInstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String current = Paths.get(".").toAbsolutePath().normalize().toString()+"/";
		String baseOriginDir = current + "/origin/";
		String filename = baseOriginDir + "Example.java";
		
		response.setContentType("text/html");
		request.setAttribute("class", Utils.readTimeFile(filename));
		request.getRequestDispatcher("/view/instrumentation_simplified.jsp").forward(request, response);
		
	}

}
