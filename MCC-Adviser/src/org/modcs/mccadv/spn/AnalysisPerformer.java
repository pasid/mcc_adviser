package org.modcs.mccadv.spn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.modcs.mccadv.input.CreateSpn;
import org.modcs.mccadv.utils.Constants;
import org.modcs.mccadv.utils.MetricEnum;
import org.modcs.mccadv.utils.PropertiesReader;
import org.modcs.mccadv.utils.StatisticTypeEnum;
import org.modcs.tools.parser.model.metrics.Metric;
import org.modcs.tools.parser.model.metrics.SPNMeanTimeToAbsorptionMetric;
import org.modcs.tools.parser.model.metrics.SPNStationaryAnalysisMetric;
import org.modcs.tools.parser.model.metrics.SPNTransientAnalysisMetric;
import org.modcs.tools.parser.model.metrics.SPNTransientSimulationMetric;

import advisor.main.MethodAdvisor;
import method.advisor.ClassNode;

public class AnalysisPerformer {

	private static PrintWriter out;
	
//	public static void runFullAnalysis(MetricEnum metric) throws Exception, IOException {
//		System.out.println("Starting "+ metric.toString() + " evaluation...\n");
//	
//		AnalysisPerformer.deleteOldLogs();
//	
//		ClassNode cn = MethodAdvisor.runMethodAdvisor("input_methods.java");
//		CreateSpn createSPN = new CreateSpn();
//		createSPN.createMethods(cn);
//		
//		if (metric.equals(MetricEnum.ENERGY)) {
//			createSPN.readInputFile(0, MetricEnum.TIME);
//			createSPN.readInputFile(0, MetricEnum.ENERGY);
//			AnalysisPerformer.executeAnalisysToPlotInBarPlot(createSPN, StatisticTypeEnum.MCETE);
//			createSPN.generateBarChart(StatisticTypeEnum.MCETE);
//		}else{
//			createSPN.readInputFile(0, MetricEnum.TIME);
//			
//			AnalysisPerformer.executeAnalisysToPlotInBarPlot(createSPN, StatisticTypeEnum.THROUGHPUT);
//			createSPN.generateBarChart(StatisticTypeEnum.THROUGHPUT);
//
//			AnalysisPerformer.executeAnalisysToPlotInBarPlot(createSPN, StatisticTypeEnum.MTTE);
//			createSPN.generateBarChart(StatisticTypeEnum.MTTE);
//			
//			AnalysisPerformer.executeASetOfTransitentSimulation_LinePlot(createSPN, 0);
//			createSPN.generateLineChart();
//		}
//	}
	
	/** Execute Transient Analysis or Simulation, depends on execution mode.
	 * 
	 * @param createSpn			Object with data about SPN model.
	 * @param resourceNumber			Amount of resources on Resource Pool's place.
	 */
	public static synchronized void executeASetOfTransitentSimulation_LinePlot(CreateSpn createSPN, int resourceNumber) throws Exception {
		System.out.println("Calculating CDF");
		createSPN.setResultsTransient(new ArrayList<ArrayList<ResultTime>>());
		if (PropertiesReader.FIXED_CONFIGURATION) {
			//usado quando eu quero simular com um mesmo modelo diferentes quantidades de recursos com valores pre estabelecidos
				for (int i = 0; i < PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER.length; i++) {
					executeASetOfTransitentSimulation_LinePlot_Aux(createSPN, PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER[i]);
				}
		}else{
			for (int i = 1; i <= createSPN.getCountParalellism(); i++) {
				executeASetOfTransitentSimulation_LinePlot_Aux(createSPN, i);
			}
		}
		createSPN.generateLineChart();
	}
	/** Create the SPN Model for Transient Analysis and Simulation, and creates the probabilities logs. 
	 * 
	 * @param createSpn			Object with data about SPN model.
	 * @param resourceNumber			Amount of resources on Resource Pool's place.
	 */
	private static void executeASetOfTransitentSimulation_LinePlot_Aux(CreateSpn createSPN, int resourceNumber) throws FileNotFoundException, Exception, IOException {
		System.err.print("\nNumber of paralell processes " + createSPN.getCountParalellism() + "\n");
		System.err.print("\nExecution with " + resourceNumber + " resource(s).");
		System.err.println("Limit Time: " + createSPN.getLimitTime() + ", Inner Step: " + createSPN.getInnerStepForSimulation() + ".");
		System.err.println("....");
	
		if (PropertiesReader.FIXED_CONFIGURATION) {
			out = new PrintWriter(Constants.RESULTS_FOLDER + FileSystems.getDefault().getSeparator() + "logTransientSimulation_" + resourceNumber + "_resource.txt");
		} else {
			out = new PrintWriter("logTransientSimulation_" + resourceNumber + "_resource.txt");
		}
		SpnStructure spnStructure = new SpnStructure(createSPN.methodList, resourceNumber);
		spnStructure.createBasicSPNStructure();
		spnStructure.addResourcePool();
		spnStructure.createArcsTransientAnalysis();
		ArrayList<ResultTime> resultTimeList = executeTransientSimulation_LinePlot(createSPN, spnStructure);
		createSPN.getResultsTransient().add(resultTimeList);
		out.close();
	}

	/** Performs a series of Transient Analyzes or Transient Analyzes depends on execution mode, calculates until the probability reach 1.
	 * 
	 * @param createSpn			Object with data about SPN model.
	 * @param spnStructure			The SPN model.
	 * @return An ArrayList with time and probability paired.
	 */
	// Realiza a Simula��o Transiente
	private static synchronized ArrayList<ResultTime> executeTransientSimulation_LinePlot(CreateSpn createSpn, SpnStructure spnStructure) throws IOException {
		ArrayList<ResultTime> resultados = new ArrayList<ResultTime>();
		double result = 0;

		String metric = getMetricLastCall(spnStructure);
		double specificTime = 0; // Representa o tempo para o qual est� ocorrendo a execu��o.

		Metric m = null;

		while (result < 0.9899) {
			if (PropertiesReader.FIXED_CONFIGURATION) {
				SPNTransientSimulationMetric.setTransientParameters(PropertiesReader.SIMULATION_CONFIDENCE_LEVEL, PropertiesReader.SIMULATION_MAX_RELATIVE_ERROR, PropertiesReader.SIMULATION_SAMPLING_POINTS,
						PropertiesReader.SIMULATION_MAX_SIMULATION_REAL_TIME);
				m = SPNTransientSimulationMetric.getMetric(spnStructure.getModel(), metric, specificTime);
			}else{
				SPNTransientAnalysisMetric.setTransientAnalysisParameters(0.1, 0.000001);
				m = SPNTransientAnalysisMetric.getMetric(spnStructure.getModel(), metric, specificTime);
			}

			spnStructure.getModel().addMetric(m);
			result = m.solve();

			System.out.println("Time: " + specificTime + " Prob: " + result);
			out.write(specificTime + ";" + result);
			out.println();
			
			resultados.add(new ResultTime(specificTime, result));
			specificTime += createSpn.getInnerStepForSimulation();
		}

		if(result >= 0.9899){
			int u = 0;
			while(u < 3){
				specificTime += createSpn.getInnerStepForSimulation();
				resultados.add(new ResultTime(specificTime, 1));
				u++;
			}
		}

		return resultados;
	}

	/** Construct the string with metric used on Transient Analysis and Transient Simulation.
	 * 
	 * @param spnStructure			The SPN model.
	 * @return The metric's string.
	 */
	private static String getMetricLastCall(SpnStructure spnStructure) {

		String metric = "";

		if(spnStructure.getModel().getPlace(Constants.NAME_LAST_PLACE) == null){
			metric = "P{#" + spnStructure.getLastPlaceLastCallName() + ">0}"; // Define a m�trica
		} else {
			metric = "P{#" + Constants.NAME_LAST_PLACE + ">0}"; // Define a m�trica
		}
		return metric;
	}

	
//	public static void executeAnalisysToPlotInBarPlot(CreateSpn createSpn, StatisticTypeEnum statisticType) throws Exception{
//		if (statisticType.equals(StatisticTypeEnum.MTTE)) {
//			executeMeanTimeToAbsorption_BarPlot_Fixed(createSpn);
//		}else if (statisticType.equals(StatisticTypeEnum.MCETE) || statisticType.equals(StatisticTypeEnum.THROUGHPUT)) {
//			executeStationaryAnalisys_BarPlotAux(createSpn, statisticType);
//		}
//		createSpn.generateBarChart(statisticType);
//	}
//	
	
//	/**
//	 * Used for calculating throughput and mtte 
//	 * @param createSpn
//	 * @param statisticType
//	 * @throws Exception
//	 */
//	private static void executeStationaryAnalisys_BarPlotAux(CreateSpn createSpn, StatisticTypeEnum statisticType) throws Exception {
//
//		if (statisticType == StatisticTypeEnum.THROUGHPUT) {
//			System.out.println("Calculating "+ StatisticTypeEnum.THROUGHPUT.getType() +" ...");
//		}else if (statisticType == StatisticTypeEnum.MCETE) {
//			System.out.println("Calculating "+ StatisticTypeEnum.MCETE.getType()+" ...");
//		}
//		
//		
//		createSpn.setResultsStationary(new double[createSpn.getCountParalellism() + PropertiesReader.EXTRA_BARS_NUMBER]);
//
//		for (int resourceNumber = 1; resourceNumber <= createSpn.getCountParalellism() + PropertiesReader.EXTRA_BARS_NUMBER; resourceNumber++) {
//			SpnStructure spnStructure = new SpnStructure(createSpn.methodList, resourceNumber);
//
//			spnStructure.createBasicSPNStructure(); // Create the structure representating a Method on SPN
//			spnStructure.addResourcePool(); // Create the Resource Pool Place
//			spnStructure.createArcsStationayAnalisys(); // Create and connect Arcs
//
//			double result_r1 = 0;
//			if (statisticType == StatisticTypeEnum.THROUGHPUT) {
//				result_r1 = solveThroughput(spnStructure);
//			}else if (statisticType == StatisticTypeEnum.MCETE) {
//				result_r1 = solveMCETE(spnStructure, createSpn);
//			}
//			
//			DecimalFormat df = new DecimalFormat("0.000000"); //Used to format the return for the chart plot.
//			result_r1 = Double.valueOf((df.format(result_r1).replace(',', '.')));
//			
//			createSpn.getResultsStationary()[resourceNumber - 1] = result_r1;
//		}
//	}
	
	public static void executeAnalysis_to_BarPlot_Fixed(CreateSpn createSPN, StatisticTypeEnum statisticType) throws Exception {

		if (statisticType == StatisticTypeEnum.MTTE) {
			System.out.println("Calculating "+ StatisticTypeEnum.MTTE.getType()+" ...");
			createSPN.setResultMeanToTimeAbsortion(new double[PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER.length]);
		}else if (statisticType == StatisticTypeEnum.THROUGHPUT) {
			System.out.println("Calculating "+ StatisticTypeEnum.THROUGHPUT.getType()+" ...");
			createSPN.setResultsStationary(new double[PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER.length]);
		}else if (statisticType == StatisticTypeEnum.MCETE) {
			System.out.println("Calculating "+ StatisticTypeEnum.MCETE.getType()+" ...");
			createSPN.setResultsStationary(new double[PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER.length]);
		}

		for (int i = 0; i < PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER.length; i++) {
			SpnStructure spnStructure = new SpnStructure(createSPN.methodList, PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER[i]);

			spnStructure.createBasicSPNStructure(); // Create the structure representating a Method on SPN
			spnStructure.addResourcePool(); // Create the Resource Pool Place
			
			DecimalFormat df = new DecimalFormat("0.000000"); // Para formatar a sa�da da an�lise para a cria��o do gr�fico.

			
			double result_r1;
			if (statisticType == StatisticTypeEnum.MTTE) {
				spnStructure.createArcsTransientAnalysis(); // Create and connect Arcs
				
				result_r1 = solveMTTE(spnStructure);
				result_r1 = Double.valueOf((df.format(result_r1).replace(',', '.')));
				createSPN.getResultMeanToTimeAbsortion()[i] = result_r1;
				System.out.println("MTTE calculated to " + PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER[i] + " Resources Equals to "+ result_r1);
				
			}else if (statisticType == StatisticTypeEnum.THROUGHPUT) {
				spnStructure.createArcsStationayAnalisys(); // Create and connect Arcs
				
				result_r1 = solveThroughput(spnStructure);
				result_r1 = Double.valueOf((df.format(result_r1).replace(',', '.')));
				createSPN.getResultsStationary()[i] = result_r1;
				System.out.println("THROUGHPUT calculated to " + PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER[i] + " Resources Equals to "+ result_r1);
			}else if (statisticType == StatisticTypeEnum.MCETE) {
				spnStructure.createArcsStationayAnalisys(); // Create and connect Arcs
				
				result_r1 = solveMCETE(spnStructure, createSPN);
				result_r1 = Double.valueOf((df.format(result_r1).replace(',', '.')));
				createSPN.getResultsStationary()[i] = result_r1;
				System.out.println("MCETE calculated to " + PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER[i] + " Resources Equals to "+ result_r1);
			}
		}
		createSPN.generateBarChart(statisticType);
	}

	/** Calculates the Stationary Analysis.
	 * 
	 * @param spnStructure			The SPN model.
	 * @return			The throughtput.
	 */
	private static double solveThroughput(SpnStructure spnStructure) {

		String metric = "P{#INACTIVE_PLACE>0}*(1/0.0001)";
		Metric m = SPNStationaryAnalysisMetric.getMetric(spnStructure.getModel(), metric);
		spnStructure.getModel().addMetric(m);
		double result = m.solve();

		return result;
	}
	
	private static double solveMCETE(SpnStructure spnStructure, CreateSpn createSpn) {

		String MCETE = String.valueOf(createSpn.getTotalTime())+ "*(";
		String summUp = "";
		
		for (Method method : spnStructure.getMethods()) {
			summUp+= "((P{#"+method.getNameOfIntermediaryPlace() +">0}*1/"+method.getTime()+")*"+method.getEnergy()+")";
			if ((spnStructure.getMethods().indexOf(method)+1)!=spnStructure.getMethods().size()) {
				summUp += "+";
			}
		}
		
		MCETE +=  summUp + ")";
		System.out.println("MCETE Metric: "+ MCETE);
		System.out.println("Number of Resources: " + spnStructure.getQuantityOfResource() +"\n");
		Metric m = SPNStationaryAnalysisMetric.getMetric(spnStructure.getModel(), MCETE);
		spnStructure.getModel().addMetric(m);
		double result = m.solve();
		System.out.println("MCETE Result: " + result);
		return result;
	}
	
	/** Execute Mean Time To Absortion metric.
	 * 
	 * @param spnStructure			The SPN model.
	 * @return The time needed to make the execution.
	 */	
	private static double solveMTTE(SpnStructure spnStructure) {
		Metric m = SPNMeanTimeToAbsorptionMetric.getMetric(spnStructure.getModel());

		double solve = m.solve();
//		System.out.println("MTTE: " + solve);
		return solve;
	}
	

	/** This method gives the average probability by given a time interval.
	 * 
	 * @param interval_1			The first time.
	 * @param interval_2			The second time. If the input equals to 0, will be considered the maximum time from log.
	 * @return			The average probabilities.
	 */
	public static ArrayList<String> getProbabilityByTime (double interval_1, double interval_2) throws IOException{

		boolean exist = true;
		int j = 1;
		ArrayList<String> retorno = new ArrayList<String>();


		while(exist){
			File file = new File("logTransientSimulation_" + j +"_resource.txt");
			if(file.exists()){

				String firstLine = "";
				String lastLine = "";

				FileInputStream fis = new FileInputStream(file);
				InputStreamReader isrTime = new InputStreamReader(fis);
				BufferedReader brTime = new BufferedReader(isrTime);

				double probability_1 = -1;
				double probability_2 = -1;

				String line = brTime.readLine();
				String line_2 = brTime.readLine();

				//Get first time
				String[] firstTimeArray = line.split(";");
				firstLine = firstTimeArray[0];

				lastLine = getTimeLimits("logTransientSimulation_" + j +"_resource.txt");

				int i = 0;

				if(Double.valueOf(firstLine) > interval_1 || Double.valueOf(firstLine) > interval_2 || (interval_1 > interval_2 && interval_2 != 0)){ //Invalid combination of times are inserted
					return null;
				} else {
					while (line != null && line_2 != null) {

						String[] tabSeparatedArray = line.split(";");
						String[] tabSeparatedArray_2 = line_2.split(";");

						if (interval_2 == 0 && interval_1 != 0){ //User put only the first time
							if(probability_1 == -1){ 
								if(Double.valueOf(lastLine) < interval_1){
									probability_1 = 1;
								} else {
									if(interval_1 >= Double.parseDouble(tabSeparatedArray[0]) && interval_1 < Double.parseDouble(tabSeparatedArray_2[0])){

										double dif_1 = interval_1 - Double.parseDouble(tabSeparatedArray[0]);
										double dif_2 = Double.parseDouble(tabSeparatedArray_2[0]) - interval_1;

										if(dif_1 < dif_2){
											probability_1 = Double.parseDouble(tabSeparatedArray[1]);
										}else{
											probability_1 = Double.parseDouble(tabSeparatedArray_2[1]);
										}
										
									}
								}
								probability_2 = 1;
							}
							
							if(probability_1 != -1 && probability_2 != -1){ //Find final probability
								if((probability_2 - probability_1) != 0){
									if((probability_2 - probability_1) < 0){
										retorno.add(String.valueOf(((probability_2 - probability_1)*-1)*100));
									}else{
										retorno.add(String.valueOf((probability_2 - probability_1)*100));
									}	
								}  else {
									retorno.add(String.valueOf(probability_2*100));
								}
								break;
							}
							
							i++;
							line = line_2;
							line_2 = brTime.readLine();

						} else { //User entered two times

							if(probability_1 == -1){ //Find the probability for the first time
								if(Double.valueOf(lastLine) < interval_1){
									probability_1 = 1;
								} else {
									if(interval_1 >= Double.parseDouble(tabSeparatedArray[0]) && interval_1 < Double.parseDouble(tabSeparatedArray_2[0])){
										double dif_1 = interval_1 - Double.parseDouble(tabSeparatedArray[0]);
										double dif_2 = Double.parseDouble(tabSeparatedArray_2[0]) - interval_1;

										if(dif_1 < dif_2){
											probability_1 = Double.parseDouble(tabSeparatedArray[1]);
										}else{
											probability_1 = Double.parseDouble(tabSeparatedArray_2[1]);
										}
									}
								}
							}

							if(probability_1 != -1 && probability_2 == -1){ //Find the probability for the second time

								if(Double.valueOf(lastLine) < interval_2){
									probability_2 = 1;
								} else {
									if(interval_2 >= Double.parseDouble(tabSeparatedArray[0]) && interval_2 < Double.parseDouble(tabSeparatedArray_2[0])){
										double dif_1 = interval_2 - Double.parseDouble(tabSeparatedArray[0]);
										double dif_2 = Double.parseDouble(tabSeparatedArray_2[0]) - interval_2;

										if(dif_1 < dif_2){
											probability_2 = Double.parseDouble(tabSeparatedArray[1]);
										}else{
											probability_2 = Double.parseDouble(tabSeparatedArray_2[1]);
										}
									}
								}
							}

							if(probability_1 != -1 && probability_2 != -1){ //Find final probability
								if((probability_2 - probability_1) != 0){
									if((probability_2 - probability_1) < 0){
										retorno.add(String.valueOf(((probability_2 - probability_1)*-1)*100));
									}else{
										retorno.add(String.valueOf((probability_2 - probability_1)*100));
									}	
								}  else {
									retorno.add(String.valueOf(probability_2*100));
								}
								break;
							}

							i++;
							line = line_2;
							line_2 = brTime.readLine();

						}

					}
					brTime.close();
				}

			} else {
				exist = false;
			}
			j++;
		}
		
		//Ensures that no probability is greater than 100%
		for (int i = 0; i < retorno.size(); i++){
			if(Double.valueOf(retorno.get(i)) > 100){
				retorno.set(i, "100");
			}
		}
		return retorno;
	}

	/** Execute Mean Time To Absorption.
	 * 
	 * @param ir			Object with data about SPN model.
	 */
	private static void executeMeanTimeToAbsorption_BarPlot(CreateSpn ir) throws Exception {
		System.out.println("Calculating MTTE");

		ir.setResultMeanToTimeAbsortion(new double[PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER.length]);

		for (int resourceNumber = 1; resourceNumber <= ir.getCountParalellism() + PropertiesReader.EXTRA_BARS_NUMBER; resourceNumber++) {
			SpnStructure spnStructure = new SpnStructure(ir.methodList, resourceNumber);

			spnStructure.createBasicSPNStructure(); // Create the structure representating a Method on SPN
			spnStructure.addResourcePool(); // Create the Resource Pool Place
			spnStructure.createArcsTransientAnalysis(); // Create and connect Arcs

			DecimalFormat df = new DecimalFormat("0.000000"); // Para formatar a sa�da da an�lise para a cria��o do gr�fico.
			double result_r1 = solveMTTE(spnStructure);

			result_r1 = Double.valueOf((df.format(result_r1).replace(',', '.')));
			ir.getResultMeanToTimeAbsortion()[resourceNumber - 1] = result_r1;
		}
	}
	
	private static void executeMeanTimeToAbsorption_BarPlot_Fixed(CreateSpn ir) throws Exception {
		System.out.println("Calculating MTTE");

		ir.setResultMeanToTimeAbsortion(new double[PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER.length]);

		for (int i = 0; i < PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER.length; i++) {
			SpnStructure spnStructure = new SpnStructure(ir.methodList, PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER[i]);

			spnStructure.createBasicSPNStructure(); // Create the structure representating a Method on SPN
			spnStructure.addResourcePool(); // Create the Resource Pool Place
			spnStructure.createArcsTransientAnalysis(); // Create and connect Arcs

			DecimalFormat df = new DecimalFormat("0.000000"); // Para formatar a sa�da da an�lise para a cria��o do gr�fico.
			double result_r1 = solveMTTE(spnStructure);

			result_r1 = Double.valueOf((df.format(result_r1).replace(',', '.')));
			ir.getResultMeanToTimeAbsortion()[i] = result_r1;
			System.out.println("MTTE calculated to " + PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER[i] + " Resources Equals to "+ result_r1);
		}
	}

	

	/** Delete old log files.
	 * 
	 */		
	public static void deleteOldLogs() throws Exception{

		boolean exist = true;
		int j = 1;
		while(exist){
			File file = new File("logTransientSimulation_" + j +"_resource.txt");

			if(file.exists()){
				if(file.delete()){
					j++;
				} else {
					throw new Exception();
				}
			} else {
				exist = false;
			}
		}

	}
	/** Get the last time from a specific probability log file.
	 * 
	 * @param logName The name of file.
	 */	
	public static String getTimeLimits(String logName) throws IOException{

		FileInputStream fis = new FileInputStream(new File(logName));
		InputStreamReader isrTime = new InputStreamReader(fis);
		BufferedReader brTime = new BufferedReader(isrTime);
		String lastLine = "";
		String currentLine = "";

		while ((currentLine = brTime.readLine()) != null) 
		{
			String[] tabSeparatedArray = currentLine.split(";");
			lastLine = tabSeparatedArray[0];
		}


		brTime.close();
		isrTime.close();

		return lastLine;
	}

	/** Calculate the progress of Transient Analysis
	 * 
	 * @param probability The probability returned by previous Analysis calculus.
	 * @param index The index to help on progress calculus.
	 * @param qtdResources Amount of resources on Resource Pool's place..
	 */	
	public static double progress (double probability, int index, int qtdResources){

		if(index == 1){
			return (probability * 100)/qtdResources;
		} else {
			return (((probability + 1) * 100) * (index-1))/qtdResources;
		}
	}
}