package br.ufpe.cin.modcs.mccadviser.testbed.mccclient.instrumenter;

import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.App;

public class EnergyLogger extends Logger
{
    private ConsumptedEnergyReader energyReader;

    private double totalEnergyConsumption = 0;
    private boolean logIsFinished;


    public void registerStart()
    {
        energyReader = new ConsumptedEnergyReader();
        energyReader.start();
    }

    public double registerFinishing()
    {
        logIsFinished = true;

        return Math.abs(totalEnergyConsumption/1000000000000.0);  //TOTAL_ENERGY_CONSUMPTION_IN_JOULES
    }

    class ConsumptedEnergyReader extends Thread
    {
        @Override
        public void run()
        {
            while (!logIsFinished)
            {
                long voltage = SystemSpecialFileReader.getReadLongDataFromFile(App.INSTANTENOUS_VOLTAGE_SYSTEM_FILE);
                long current = SystemSpecialFileReader.getReadLongDataFromFile(App.INSTANTENOUS_CURRENT_SYSTEM_FILE);

                totalEnergyConsumption += (voltage*current) * 500/1000.0;

                //Log.e("MCC", "voltage=" + voltage + ";current=" + current + ";totalEnergyConsumption=" + (totalEnergyConsumption/1000000000000.0));

                try
                {
                    Thread.sleep(500); // Delay of 500 milliseconds
                }
                catch(Exception e)
                {
                }
            }
        }
    }
}