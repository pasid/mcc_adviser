package org.modcs.mccadv.input;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;

import method.advisor.ClassNode;
import method.advisor.Dependence;
import method.advisor.MethodNode;

import org.modcs.mccadv.chart.BarChartGenerator;
import org.modcs.mccadv.chart.LineChartGenerator;
import org.modcs.mccadv.spn.Method;
import org.modcs.mccadv.spn.ResultTime;
import org.modcs.mccadv.utils.Constants;
import org.modcs.mccadv.utils.MetricEnum;
import org.modcs.mccadv.utils.PropertiesReader;
import org.modcs.mccadv.utils.StatisticTypeEnum;

public class CreateSpn {

	public InputStreamReader isr;
	public BufferedReader br;
	public ArrayList<Method> methodList; // Lista de Method (estrutura que representam os m�todos).
	private int countParalellism; // Grau de paralelismo da entrada.
	private List<ArrayList<ResultTime>> resultsTransient; // Lista de resultados da an�lise transiente.
	private double resultsStationary[]; // Lista de resultados da an�lise estacion�ria.
	private double resultMeanToTimeAbsortion[]; // Lista de resultados da an�lise estacion�ria.
	private double sumOfAllTransientTime; // Soma dos tempos das chamadas. Usado para saber o limite de tempo na simula��o.
	private double sumOfAllTransientEnergy; // Soma dos energias das chamadas. Usado para saber o limite de energia na simula��o.
	private double totalTime; // Usado para calcular MCETE

	public CreateSpn() {
		
		this.methodList = new ArrayList<Method>();
		this.countParalellism = 0;
		this.sumOfAllTransientTime = 0;

		paralellism();
	}
	/**
	 * Read and interprets input_methods.java file, making a list of Method (structure that represent method call) and yours dependencies.
	 * @param cn Object with Method dependence structure.
	 * @throws IOException
	 */

	public void createMethods(ClassNode cn) throws IOException {

		for (MethodNode mn : cn.getMethodNodes()) {
			for (Dependence d : mn.getDependences()) {

				Method method1 = null;
				Method method2 = null;
				if (d.getAssign() != null) {
					method1 = searchMethod(d.getAssign().toString());
					if (method1 == null) {
						method1 = new Method();
						method1.setName(d.getAssign().toString());
						this.methodList.add(method1);
					}
				}

				if (d.getCall() != null) {
					method2 = searchMethod(d.getCall().toString());
					if (method2 == null) {
						method2 = new Method();
						method2.setName(d.getCall().toString());
						if (method1 != null) {
							method2.setDependsOn(method1);
						}
						this.methodList.add(method2);
					} else {
						if (method1 != null) {
							method2.setDependsOn(method1);
						}

					}
					if (method1 != null) {
						method1.setDependents(method2);
					}
				}
			}
		}
		paralellism();
	}

	/**
	 * Put the delay or energy metric on Methods.
	 * @param resourcesNumber Number of resources on Resource Pool place.
	 * @throws IOException
	 * @throws NumberFormatException
	 * @throws ArrayIndexOutOfBoundsException
	 */
	public void readInputFile(int resourcesNumber, MetricEnum metric) throws IOException, NumberFormatException, ArrayIndexOutOfBoundsException {
		// try{
		
		String fileName;
		if (metric.equals(MetricEnum.TIME)) {
			fileName = Constants.INPUT_TIME_FILE_NAME;
		}else{
			fileName = Constants.INPUT_ENERGY_FILE_NAME;
		}

		FileInputStream fis = null;
		if(PropertiesReader.FIXED_CONFIGURATION){
			fis = new FileInputStream(new File(Constants.AUX_FILES_FOLDER+FileSystems.getDefault().getSeparator()+resourcesNumber+"_resources_"+fileName));
		} else {
			fis = new FileInputStream(new File(fileName));
		}
		
		InputStreamReader isrTime = new InputStreamReader(fis);
		BufferedReader brTime = new BufferedReader(isrTime);

		String line = brTime.readLine();
		int i = 0;
		while (line != null) {

			String[] tabSeparatedArray = line.split("=");

			if (line.contains("totalTime")) {
				this.setTotalTime(Double.parseDouble(tabSeparatedArray[1]));
			}else{
				if (metric.equals(MetricEnum.TIME)) {
					this.methodList.get(i).setTime(Double.parseDouble(tabSeparatedArray[1]));
					this.sumOfAllTransientTime += Double.parseDouble(tabSeparatedArray[1]);
				}else if(metric.equals(MetricEnum.ENERGY)) {
					this.methodList.get(i).setEnergy(Double.parseDouble(tabSeparatedArray[1]));
					this.sumOfAllTransientEnergy += Double.parseDouble(tabSeparatedArray[1]);
				}
			}
			
			

			i++;
			line = brTime.readLine();
		}

		brTime.close();
	}
	
	/**
	 * Verify the maximum parallelism level of input file.
	 */
	public void paralellism() {
		for (int i = 0; i < this.methodList.size(); i++) {
			if (this.methodList.get(i).getDependsOn().size() == 0) {
				this.countParalellism++;
			}
		}
	}
	/**
	 * Search for a Method by the method call name.
	 * @param methodName The name of method call.
	 * @return The Method.
	 */
	public Method searchMethod(String methodName) {
		for (int i = 0; i < this.methodList.size(); i++) {
			if (this.methodList.get(i).getName().equals(methodName)) {
				return this.methodList.get(i);
			}
		}
		return null;
	}

	/**
	 * Generate a bar chart for Stationary Analysis.
	 */
	public void generateBarChart(StatisticTypeEnum statisticTypeEnum) {
		if (statisticTypeEnum.equals(StatisticTypeEnum.THROUGHPUT) || 
				statisticTypeEnum.equals(StatisticTypeEnum.MCETE)) {
			new BarChartGenerator(this.resultsStationary, statisticTypeEnum.getType()+"Chart").makeStationaryChart(statisticTypeEnum);
		}else if (statisticTypeEnum.equals(StatisticTypeEnum.MTTE)){
			new BarChartGenerator(this.resultMeanToTimeAbsortion, statisticTypeEnum.getType()+"Chart").makeStationaryChart(statisticTypeEnum);
		}
		System.out.println("Bar Chart " + statisticTypeEnum + " just generated.");
	}

//	/**
//	 * Generate a bar chart for MTTA.
//	 */
//	public void generateBarChartMTTA() {
//		new BarChartGenerator(this.resultMeanToTimeAbsortion, "MTTEChart").makeMTTAChart();
//	}
//	
	
	/**
	 * Generate a line chart for Transient Stationary.
	 */
	public void generateLineChart() {
		new LineChartGenerator(this.resultsTransient, StatisticTypeEnum.CDF.getType()+"Chart").makeStationaryChart();
	}
	
	/**
	 * Return a list with all Methods.
	 * @return Method list
	 */
	public ArrayList<Method> getMethodList() {
		return methodList;
	}
	
	/**
	 * Set a Method list.
	 */
	public void setMethodList(ArrayList<Method> methodList) {
		this.methodList = methodList;
	}
	
	/**
	 * Get parallelism level.
	 * @return Parallelism level.
	 */
	public int getCountParalellism() {
		return countParalellism;
	}
	/**
	 * Set parallelism level.
	 * @param countParalellism Parallelism level.
	 */
	public void setCountParalellism(int countParalellism) {
		this.countParalellism = countParalellism;
	}
	
	/**
	 * Get the list with Transient Analysis results.
	 * @return  Transient Analysis results.
	 */
	public List<ArrayList<ResultTime>> getResultsTransient() {
		return resultsTransient;
	}
	
	/**
	 * Set the list with Transient Analysis results.
	 * @param resultsTransient Transient Analysis results.
	 */
	public void setResultsTransient(List<ArrayList<ResultTime>> resultsTransient) {
		this.resultsTransient = resultsTransient;
	}

	/**
	 * Get the list with Stationary Analysis results.
	 * @return  Stationary Analysis results.
	 */
	public double[] getResultsStationary() {
		return resultsStationary;
	}
	/**
	 * Set the list with Stationary Analysis results.
	 * @param resultsStationary Stationary Analysis results.
	 */
	public void setResultsStationary(double[] resultsStationary) {
		this.resultsStationary = resultsStationary;
	}
	
	/**
	 * Get the list with Mean To Time Absortion results.
	 * @return  Mean To Time Absortion results.
	 */
	public double[] getResultMeanToTimeAbsortion() {
		return resultMeanToTimeAbsortion;
	}
	/**
	 * Set the list with Mean To Time Absortion results.
	 * @param resultMeanToTimeAbsortion Mean To Time Absortion results.
	 */
	public void setResultMeanToTimeAbsortion(double[] resultMeanToTimeAbsortion) {
		this.resultMeanToTimeAbsortion = resultMeanToTimeAbsortion;
	}

	/**
	 * Get the increment factor for adding in specific time on Transient Analysis calculation.
	 * @return The increment factor.
	 */
	public double getInnerStepForSimulation() {
		if (PropertiesReader.FIXED_CONFIGURATION) {
			return PropertiesReader.FIXED_INNER_STEP;
		}else{
			return this.sumOfAllTransientTime * PropertiesReader.PERCENTAGE_INNER_STEP;
		}
	}

	/**
	 * Get the maximum time for looping in Transient Analysis
	 * @return The maximum time.
	 */
	public double getLimitTime() {
		if (PropertiesReader.FIXED_CONFIGURATION) {
			return PropertiesReader.FIXED_LIMIT_TIME;
		}else{
			return this.sumOfAllTransientTime * PropertiesReader.MULTIPLICITY_FOR_LIMIT_TIME;
		}
	}
	public double getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(double totalTime) {
		this.totalTime = totalTime;
	}
	
	

}
