package com.redraccoon.dijkstra.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import com.redraccoon.dijkstra.model.Edge;
import com.redraccoon.dijkstra.model.Graph;

/**
 * Client code to demonstrate Dijkstra's algorithm
 * 
 * @author Stewart Wright
 *
 */
public class DijkstraClient {
	
	public static void main(String[] args) {
	
		
		int[] numberOfEdges = {8000, 21000, 7000};
		
		for (int i : numberOfEdges) {
			checkBytes(i);
		}
		
	
	}
	
	public static Graph getGraph(int numberOfEdges) {
		java.util.List<Edge> edges = new ArrayList<>();
		for (int i = 0; i < numberOfEdges; i++) {
			edges.add(new Edge(4, 4, 8));
		}
		
		Edge[] array = new Edge[edges.size()];
		edges.toArray(array); // fill the array
		
		return new Graph(array);
	}


	public static void checkBytes(int numberOfEdges) {
		java.util.List<Edge> edges = new ArrayList<>();
		for (int i = 0; i < numberOfEdges; i++) {
			edges.add(new Edge(4, 4, 8));
		}
		
		Edge[] array = new Edge[edges.size()];
		edges.toArray(array); // fill the array
		
		Graph graph = new Graph(array);
		

		printBytesSize(graph, numberOfEdges);
//		graph.calculateShortestDistances();
//		System.out.println(graph.toString());
	}

	public static void printBytesSize(Graph graph, int size) {
		byte[] yourBytes = null;
		
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutput out = new ObjectOutputStream(bos);   
			out.writeObject(graph);
			yourBytes = bos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		  
		System.out.println("Bytes size: "+yourBytes.length + " for "+ size +" edges.");
	}

}