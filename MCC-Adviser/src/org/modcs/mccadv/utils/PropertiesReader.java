package org.modcs.mccadv.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

	public static final int SIMULATION_MAX_SIMULATION_REAL_TIME = Integer.parseInt(getPropValues("SIMULATION_MAX_SIMULATION_REAL_TIME"));
	public static final int SIMULATION_SAMPLING_POINTS = Integer.parseInt(getPropValues("SIMULATION_SAMPLING_POINTS"));
	public static final double SIMULATION_MAX_RELATIVE_ERROR = Double.parseDouble(getPropValues("SIMULATION_MAX_RELATIVE_ERROR"));;
	public static final double SIMULATION_CONFIDENCE_LEVEL = Double.parseDouble(getPropValues("SIMULATION_CONFIDENCE_LEVEL"));
	
	public static final boolean FIXED_CONFIGURATION = Boolean.parseBoolean(getPropValues("FIXED_CONFIGURATION"));

	public static final double DELAY_TO_LAST_CALL = Double.parseDouble(getPropValues("DELAY_TO_LAST_CALL"));
	public static final double DELAY_TO_PARALLEL_CALLS = Double.parseDouble(getPropValues("DELAY_TO_PARALLEL_CALLS"));
	public static final double FIXED_LIMIT_TIME = Double.parseDouble(getPropValues("FIXED_LIMIT_TIME"));
	public static final double FIXED_INNER_STEP = Double.parseDouble(getPropValues("FIXED_INNER_STEP"));
	
	public static final double PERCENTAGE_INNER_STEP = Double.parseDouble(getPropValues("PERCENTAGE_INNER_STEP"));
	public static final int EXTRA_BARS_NUMBER = Integer.parseInt(getPropValues("EXTRA_BARS_NUMBER"));
	public static final int MULTIPLICITY_FOR_LIMIT_TIME = Integer.parseInt(getPropValues("MULTIPLICITY_FOR_LIMIT_TIME"));
	public static final int NUMBER_PARALLEL_JOBS = Integer.parseInt(getPropValues("NUMBER_PARALLEL_JOBS"));
	
	public static final int[] ARRAY_RESOURCE_POOL_NUMBER = getSeparatedValuesInt(getPropValues("ARRAY_RESOURCE_POOL_NUMBER"));
//	public static final boolean USE_ARRAY_RESOURCE_POOL = Boolean.parseBoolean(getPropValues("USE_ARRAY_RESOURCE_POOL"));

	


	private static String[] getSeparatedValuesString(String s) {
        String[] ls;
        ls = s.split(",");
        String values[] = new String[ls.length];
        for(int i=0;i<ls.length;i++)
        {
        	values[i] = ls[i];
        }
        return values;
	}
	
	private static int[] getSeparatedValuesInt(String s) {
        String[] ls;
        ls = s.split(",");
        int values[] = new int[ls.length];
        for(int i=0;i<ls.length;i++)
        {
        	values[i] = Integer.parseInt(ls[i]);
        }
        return values;
	}
	
	
public static String getCleanPath() {
    ClassLoader classLoader = Constants.class.getClassLoader();
    File classpathRoot = new File(classLoader.getResource("").getPath());

    return classpathRoot.getPath();
}

	public static String getPropValues(String property) {

		Properties prop = new Properties();
		String propFileName = Constants.PROPERTIES_FILE_NAME;

		InputStream inputStream = null;

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		inputStream = classLoader.getResourceAsStream(propFileName);
		try {
			prop.load(inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return prop.getProperty(property);
	}
}





