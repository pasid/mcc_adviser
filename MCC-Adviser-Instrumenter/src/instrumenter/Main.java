package instrumenter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;

import method.japa.parser.ParseException;

/**
 * Coloque uma classe no diret�rio dirOrigin e informe o nome do m�todo para ser instrumentado. 
 */
public class Main {

	//TODO Mudar o nome do pacote nas duas classes auxiliares no projeto destino: InstrumenterUtils e ManageFile.
	//TODO Mudar a sa�da do log para quando for um aplicativo m�vel passar usar a classe ManageFile. Para que o projeto destino seja compil�vel, neste caso ele dever� ter a lib do android. 

	public static void main(String[] args) throws FileNotFoundException, ParseException, IOException {

		String current = Paths.get(".").toAbsolutePath().normalize().toString()+"/";
	
//		String current = "D:/WORKSPACES/mcc_adviser/MCC-Adviser-Instrumenter/";
		String dirOrigin = current + "origin/";
		String dirDestiny = current+"destiny/"; // where the zip file will be copied
		String dirAuxFIles = current+"aux_inst_files/"; // where you must copy to it hte files InstrumenterUtils.java and ManageFile.java. The instrumenter will copy from this folder these files to the folder where resides the instrumented class.
		
		// this method after instrumenting and copying the file to destiny folder it clear the files inside the origin folder
		String resultOperation = Instrumenter.runInstrumenter(dirDestiny,dirOrigin, dirAuxFIles, "m");
		
		System.out.println(resultOperation);

	}

}
