package br.ufpe.cin.modcs.mccadviser.testbed.mccclient.instrumenter;

import android.content.Context;
import android.os.BatteryManager;

import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.Result;

public class BatteryLogger extends Logger
{
    private Context context;
    private Result result;

    private long initEnergy = 0;

    public BatteryLogger(Context context)
    {
        this.context = context;

        result = new Result();
    }

    public void registerStart()
    {
        initEnergy = getEnergyLevel();
    }

    public double registerFinishing()
    {
        return initEnergy - getEnergyLevel();
    }

    private Long getEnergyLevel()
    {
        BatteryManager mBatteryManager = (BatteryManager)context.getSystemService(Context.BATTERY_SERVICE);

        return mBatteryManager.getLongProperty(BatteryManager.BATTERY_PROPERTY_ENERGY_COUNTER);
    }
}
