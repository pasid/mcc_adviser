package br.ufpe.cin.modcs.mccadviser.testbed.mccclient.instrumenter;

import android.content.Context;

import java.io.IOException;

import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.ResultList;

public class LogFile
{
    private Context context;

    private Loggable logTime;
    private Loggable logEnergy;

    public LogFile(Context context)
    {
        this.context = context;

        this.logTime = new ManagerLogTime(context);
        this.logEnergy = new ManagerLogEnergy(context);
    }

    public boolean writeLog(ResultList results, double totalExecutionTime) throws IOException
    {
        boolean wasWriteLogTime = logTime.writeLog(results.getResultsOfTimeExecution(), totalExecutionTime);
        boolean wasWriteLogEnergy = logEnergy.writeLog(results.getResultsOfEnergyConsumption(), totalExecutionTime);

        return wasWriteLogTime && wasWriteLogEnergy;
    };

    public boolean clearLogFile()
    {
        boolean wasLogTimeErased = logTime.clearLogFile();
        boolean wasLogEnergyErased = logEnergy.clearLogFile();

        return wasLogTimeErased && wasLogEnergyErased;
    }
}