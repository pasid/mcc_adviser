package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class used to download the 
 * @author airton
 *
 */
public class DownloadMCCAdvServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("application/zip");
		String fileName = "MCC-Adviser.zip";
		response.setHeader("Content-disposition", "attachment; filename=\"" + fileName + "\"");
		
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Expires", "-1");

		String current = Paths.get(".").toAbsolutePath().normalize().toString()+"/";
		
		File f = new File(current+"/mcc_adv_release/"+fileName);
		byte[] arBytes = new byte[(int) f.length()];
		FileInputStream is = new FileInputStream(f);
		is.read(arBytes);
		ServletOutputStream op = response.getOutputStream();
		op.write(arBytes);
		op.flush();
	}
}