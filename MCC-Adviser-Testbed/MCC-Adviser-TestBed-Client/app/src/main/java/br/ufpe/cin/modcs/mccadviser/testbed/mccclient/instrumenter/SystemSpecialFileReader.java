package br.ufpe.cin.modcs.mccadviser.testbed.mccclient.instrumenter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.util.Log;

public class SystemSpecialFileReader
{
    /* These are stolen from Process.java which hides these constants. */
    public static final int PROC_SPACE_TERM = (int)' ';
    public static final int PROC_TAB_TERM = (int)'\t';
    public static final int PROC_LINE_TERM = (int)'\n';
    public static final int PROC_COMBINE = 0x100;
    public static final int PROC_OUT_LONG = 0x2000;
    private static final int[] READ_LONG_FORMAT = new int[] { PROC_SPACE_TERM | PROC_OUT_LONG };


    private static final int[] PROCESS_STATS_FORMAT = new int[]
    {
            PROC_SPACE_TERM,
            PROC_SPACE_TERM,
            PROC_SPACE_TERM,
            PROC_SPACE_TERM,
            PROC_SPACE_TERM,
            PROC_SPACE_TERM,
            PROC_SPACE_TERM,
            PROC_SPACE_TERM,
            PROC_SPACE_TERM,
            PROC_SPACE_TERM,
            PROC_SPACE_TERM,
            PROC_SPACE_TERM,
            PROC_SPACE_TERM,
            PROC_SPACE_TERM|PROC_OUT_LONG,                  // 13: utime
            PROC_SPACE_TERM|PROC_OUT_LONG                   // 14: stime
    };

    private static final int[] PROCESS_TOTAL_STATS_FORMAT = new int[]
    {
            PROC_SPACE_TERM,
            PROC_SPACE_TERM|PROC_OUT_LONG,
            PROC_SPACE_TERM|PROC_OUT_LONG,
            PROC_SPACE_TERM|PROC_OUT_LONG,
            PROC_SPACE_TERM|PROC_OUT_LONG,
            PROC_SPACE_TERM|PROC_OUT_LONG,
            PROC_SPACE_TERM|PROC_OUT_LONG,
            PROC_SPACE_TERM|PROC_OUT_LONG,
    };

    private static final int[] PROC_MEMINFO_FORMAT = new int[]
    {
            PROC_SPACE_TERM|PROC_COMBINE, PROC_SPACE_TERM|PROC_OUT_LONG, PROC_LINE_TERM,
            PROC_SPACE_TERM|PROC_COMBINE, PROC_SPACE_TERM|PROC_OUT_LONG, PROC_LINE_TERM,
            PROC_SPACE_TERM|PROC_COMBINE, PROC_SPACE_TERM|PROC_OUT_LONG, PROC_LINE_TERM,
            PROC_SPACE_TERM|PROC_COMBINE, PROC_SPACE_TERM|PROC_OUT_LONG, PROC_LINE_TERM,
    };

    private static long[] readBuf = new long[1];
    private static Method methodReadProcFile;

    static
    {
        try
        {
            methodReadProcFile = android.os.Process.class.getMethod("readProcFile", String.class,int[].class, String[].class, long[].class, float[].class);
        }
        catch (SecurityException e)
        {
            Log.e("DebugView", "Security Error " + e.getMessage());
        }
        catch(NoSuchMethodException e)
        {
            Log.e("DebugView", "ERROR!");
            Log.e("DebugView", e.getMessage());
        }
    }


    public static long getReadLongDataFromFile(String file)
    {
        if(methodReadProcFile == null) return -1;

        try
        {
            if((Boolean)methodReadProcFile.invoke(null, file, READ_LONG_FORMAT, null, readBuf, null))
            {
                return readBuf[0];
            }
        }
        catch(IllegalAccessException e)
        {
            Log.e("SystemSpecialFileReader", e.getMessage());
        }
        catch(InvocationTargetException e)
        {
            Log.e("SystemSpecialFileReader", e.getMessage());
        }

        return -1L;
    }
}
