/**
 * 
 */

$(document).ready(function() {
    $('#field').keyup(function() {

        var empty = false;
        $('#field').each(function() {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        if (empty) {
            $('#field_buttom').attr('disabled', 'disabled');
        } else {
            $('#field_buttom').removeAttr('disabled');
        }
    });
});