package br.ufpe.cin.modcs.mccadviser.testbed.mccclient.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class DateUtils
{
    public static String getDateTimeFormated()
    {
        android.text.format.DateFormat df = new android.text.format.DateFormat();

        return df.format("MM-dd hh:mm:ss.SSS", new java.util.Date()).toString();
    }

    public static String getTimestamp()
    {
        //android.text.format.DateFormat df = new android.text.format.DateFormat();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd hh:mm:ss.SSS", Locale.ENGLISH);

        return sdf.format(new java.util.Date()).toString();
    }
}
