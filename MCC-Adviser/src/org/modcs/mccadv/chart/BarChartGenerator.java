package org.modcs.mccadv.chart;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.jfree.chart.ChartFactory;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;

import org.jfree.chart.plot.PlotOrientation;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.modcs.mccadv.utils.PropertiesReader;
import org.modcs.mccadv.utils.StatisticTypeEnum;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

/**
 * This class create a .png image with a bar chart representing Stationary Analysis or MTTA metric.
 *
 */
public class BarChartGenerator{

	private double results [];
	private String columns [];
	private XYSeriesCollection chartData;
	private JFreeChart chart;
	private String fileName;
	
	public BarChartGenerator(double results [], String fileName) {
		// TODO Auto-generated constructor stub
		this.results = results;
		this.columns = new String [this.results.length];
		this.chartData = new XYSeriesCollection();
		this.fileName = fileName;
		
	}
	
	/**
	 * Creates the Stationary Analysis chart.
	 */
	public void makeStationaryChart(StatisticTypeEnum statisticTypeEnum){
		
		
		String rowKeyString = statisticTypeEnum.getType();
		this.chart = ChartFactory.
				createBarChart(statisticTypeEnum.getType()+ " Analysis", "Number of Resources", statisticTypeEnum.getType(), 
						createDataset(rowKeyString), PlotOrientation.VERTICAL, false, false, false);
		
		exportChart(this.fileName);
	}
	
	/**
	 * Prepare the data that will be put on charts.
	 * @param rowKeyString Y axis name.
	 * @return The data prepared.
	 */
	private CategoryDataset createDataset(String rowKeyString) {
		
		// row keys...
		final String series1 = rowKeyString;

		if (PropertiesReader.FIXED_CONFIGURATION) {
			for (int i = 0; i < PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER.length; i++) {
				final String category = PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER[i] + " Resources";
				this.columns[i] = category;
			}
		}else {
		// column keys...
		for(int i = 1; i<=this.results.length; i++){
			if(i>1){
				final String category = i + " VMs";
				this.columns[i-1] = category;
			}else{
				final String category = "1 VM";
				this.columns[i-1] = category;
			}
		  }
		}
		

		// create the dataset...
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		for(int j = 0; j<this.columns.length; j ++){
			dataset.addValue(this.results[j], series1, this.columns[j]);
		}

		return dataset;

	}
	
	/**
	 * Generate the image with the chart.
	 * @param fileName The name of file.
	 */
	public void exportChart(String fileName){

		try {
			OutputStream image = new FileOutputStream(fileName + ".png");
			ChartUtilities.writeChartAsPNG(image, this.chart, 500, 500);
			image.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
