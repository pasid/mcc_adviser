package br.ufpe.cin.modcs.mccadviser.testbed.mccclient.instrumenter;

import java.io.IOException;

public interface Loggable
{
    boolean writeLog(String log, double totalExecutionTime) throws IOException;

    boolean clearLogFile();
}