package br.ufpe.cin.modcs.mccadviser.testbed.mccclient;

public class App
{
    public static final String TAG = "MCC";

    /*
    The battery mAh value is measured as voltage from the battery. But this voltage varies from time to time.
    It drops a little when there is a high current draw and goes back when, for example the processor is idle.
    Also it changes on temperature change. So overall it is very inaccurate.
     */
    public static final String INSTANTENOUS_VOLTAGE_SYSTEM_FILE = "/sys/class/power_supply/battery/voltage_now"; //Note that voltage_now is in microvolts, not millivolts.
    public static final String INSTANTENOUS_CURRENT_SYSTEM_FILE = "/sys/class/power_supply/battery/current_now";

    public static final String LOG_TIME_FILE_NAME = "log-exec-time.csv";
    public static final String LOG_ENERGY_FILE_NAME = "log-energy.csv";
}
