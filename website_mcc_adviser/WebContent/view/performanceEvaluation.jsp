<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    
	    <!-- Custom favicon -->
	    <!-- <link rel="icon" href="../../favicon.ico"> -->
	    
	    <title>Time Evaluation</title>
	    
	    <!-- Bootstrap core CSS -->
	    <link rel="stylesheet" href="../resources/bootstrap/css/bootstrap.min.css" />
	
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <link rel="stylesheet" href="../resources/bootstrap/css/ie10-viewport-bug-workaround.css" />
	
	    <!-- Custom styles -->
	    <link rel="stylesheet" type="text/css" href="../resources/css/style.css" />
	</head>
	<body>
		<div class="site-wrapper">
			<div class="site-wrapper-inner">
				<div class="cover-container">
					
					<div class="masthead clearfix">
						<div class="inner">
							<h3 class="masthead-brand">MCC-Adviser</h3>
							<nav>
								<ul class="nav masthead-nav">
									<li class="active"><a href="#" onclick="document.location.href = '../'; return true;">Home</a></li>
									<!-- <li><a href="../view/features.jsp">Features</a></li>
									<li><a href="../view/contact.jsp">Contact</a></li> -->
								</ul>
							</nav>
						</div>
					</div>
					
					<div class="inner cover">
						<h1 class="cover-heading">Performance Evaluation</h1>
						<p class="lead">Here you can perform a time evaluation for your experiment, please feel free to try out.</p>
						<div class="well center-block formalign">
							<form>
								<!-- <div class="form-group" style="text-align:left; color: #000; text-shadow: none;">
									<label for="exampleInputFile">Please upload your java class again:</label>
									<input type="file" id="exampleInputFile">
									<p class="help-block">Example: file.java</p>
								</div> -->
								<div class="form-group" style="text-align:left; color: #000; text-shadow: none;">
									<label for="exampleInputFile">Please upload the log file generated from the code instrumentation:</label>
									<input type="file" id="exampleInputFile">
									<p class="help-block">Example: file.log</p>
								</div>
								<div class="text-left">
									<div class="btn-group">
										<input type="submit" value="Upload" name="upload" id="upload" class="btn btn-warning"/>
										<a href="#" onclick="document.location.href = '../'; return true;" class="btn btn-default">Back to main page</a>
									</div>
								</div>
							</form>
						</div>
					</div>
					
					<div class="mastfoot">
						<div class="inner">
							<p>MCC-Adviser Tool ©2016. Developed by members of <a href="http://www.modcs.org/">MoDCS</a> group.</p>
						</div>
					</div>
					
				</div>
			</div>
		</div>
    
	    <!-- Google jQuery API -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	    
	    <!-- Bootstrap core JavaScript -->
	    <script src="./resources/bootstrap/js/bootstrap.min.js"></script>
	    
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <script src="./resources/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
	</body>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    
	    <!-- Custom favicon -->
	    <!-- <link rel="icon" href="../../favicon.ico"> -->
	    
	    <title>Time Evaluation</title>
	    
	    <!-- Bootstrap core CSS -->
	    <link rel="stylesheet" href="../resources/bootstrap/css/bootstrap.min.css" />
	
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <link rel="stylesheet" href="../resources/bootstrap/css/ie10-viewport-bug-workaround.css" />
	
	    <!-- Custom styles -->
	    <link rel="stylesheet" type="text/css" href="../resources/css/style.css" />
	</head>
	<body>
		<div class="site-wrapper">
			<div class="site-wrapper-inner">
				<div class="cover-container">
					
					<div class="masthead clearfix">
						<div class="inner">
							<h3 class="masthead-brand">MCC-Adviser</h3>
							<nav>
								<ul class="nav masthead-nav">
									<li class="active"><a href="#" onclick="document.location.href = '../'; return true;">Home</a></li>
									<!-- <li><a href="../view/features.jsp">Features</a></li>
									<li><a href="../view/contact.jsp">Contact</a></li> -->
								</ul>
							</nav>
						</div>
					</div>
					
					<div class="inner cover">
						<h1 class="cover-heading">Performance Evaluation</h1>
						<p class="lead">Here you can perform a time evaluation for your experiment, please feel free to try out.</p>
						<div class="well center-block formalign">
							<form>
								<!-- <div class="form-group" style="text-align:left; color: #000; text-shadow: none;">
									<label for="exampleInputFile">Please upload your java class again:</label>
									<input type="file" id="exampleInputFile">
									<p class="help-block">Example: file.java</p>
								</div> -->
								<div class="form-group" style="text-align:left; color: #000; text-shadow: none;">
									<label for="exampleInputFile">Please upload the log file generated from the code instrumentation:</label>
									<input type="file" id="exampleInputFile">
									<p class="help-block">Example: file.log</p>
								</div>
								<div class="text-left">
									<div class="btn-group">
										<a href="#" class="btn btn-warning">Upload</a>
										<a href="#" onclick="document.location.href = '../'; return true;" class="btn btn-default">Back to main page</a>
									</div>
								</div>
							</form>
						</div>
					</div>
					
					<div class="mastfoot">
						<div class="inner">
							<p>MCC-Adviser Tool ©2016. Developed by members of <a href="http://www.modcs.org/">MoDCS</a> group.</p>
						</div>
					</div>
					
				</div>
			</div>
		</div>
    
	    <!-- Google jQuery API -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	    
	    <!-- Bootstrap core JavaScript -->
	    <script src="./resources/bootstrap/js/bootstrap.min.js"></script>
	    
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <script src="./resources/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
	</body>
</html>