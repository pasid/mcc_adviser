package br.ufpe.cin.modcs.mccadviser.testbed.mccclient.instrumenter;

import android.content.Context;
import android.util.Log;

import br.ufpe.cin.modcs.mccadviser.testbed.MethodOffloadable;
import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.App;
import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.Result;
import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.configuration.Method;
import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.configuration.Target;
import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.utils.DateUtils;
import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.utils.DeviceUtils;

public class Measurement
{
    private Logger timeLogger;
    private Logger energyLogger;
    private Result result;
    private MethodOffloadable methodOffloadable;

    private Method method;
    private int iteration;
    private int methodIndex;
    private Context context;

    public Measurement(Method method, int iteration, int methodIndex, Context context)
    {
        this.method = method;
        this.iteration = iteration;
        this.methodIndex = methodIndex;
        this.context = context;

        timeLogger = new TimeLogger();
        energyLogger = new EnergyLogger();
        this.result = new Result();
    }

    public void registerStart()
    {
        energyLogger.registerStart();
        timeLogger.registerStart();
    }

    public Result registerFinishing(Object methodReturn)
    {
        result.setEnergyConsumption(energyLogger.registerFinishing());
        result.setIteration(iteration);
        result.setMethodIndex(methodIndex);
        result.setDeviceId(DeviceUtils.getDeviceId(context));
        result.setDeviceModel(DeviceUtils.getDeviceName());
        result.setTimestamp(DateUtils.getTimestamp());
        result.setTarget(method.getTarget());

        result.setExecutionTime(timeLogger.registerFinishing());
        result.setMethodName(method.getName());


        if (method.getTarget() == Target.DEVICE)
        {
            result.setMethodReturn(methodReturn);

            if ((methodReturn.toString().indexOf("ERROR")) >= 0)
            {
                result.setError(true);
                result.setErrorMessage(methodReturn.toString());
            }
        }
        else
        if (method.getTarget() == Target.CLOUD)
        {
            if (methodOffloadable != null)
            {
                result.setExecutionTimeRemote(methodOffloadable.getExecutionTime());
                result.setMethodReturn(methodOffloadable.getMethodReturn());

                if ((methodOffloadable.isError()))
                {
                    result.setError(true);
                    result.setErrorMessage(methodOffloadable.getErrorMessage());
                }
            }
        }

        //result.setUnitEnergy("J");

        Log.d(App.TAG, "MCC LOG");
        Log.d(App.TAG, result.toString());

        return result;
    }

    public Result getResult()
    {
        return result;
    }

    public MethodOffloadable getMethodOffloadable()
    {
        return methodOffloadable;
    }

    public void setMethodOffloadable(MethodOffloadable methodOffloadable)
    {
        this.methodOffloadable = methodOffloadable;
    }

    public Method getMethod()
    {
        return method;
    }

    public int getIteration()
    {
        return iteration;
    }

    public int getMethodIndex()
    {
        return methodIndex;
    }

    public Context getContext()
    {
        return context;
    }
}