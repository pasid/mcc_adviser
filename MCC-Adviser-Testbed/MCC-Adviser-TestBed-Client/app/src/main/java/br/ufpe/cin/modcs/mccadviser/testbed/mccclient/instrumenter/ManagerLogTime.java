package br.ufpe.cin.modcs.mccadviser.testbed.mccclient.instrumenter;

import android.annotation.SuppressLint;
import android.content.Context;

import java.io.IOException;

import br.ufpe.cin.modcs.mccadviser.testbed.mccclient.App;

@SuppressLint("NewApi")
public class ManagerLogTime extends ManagerFile implements Loggable
{
    public ManagerLogTime(Context context)
    {
        super(context, App.LOG_TIME_FILE_NAME);
    }

    public ManagerLogTime()
    {
        super(App.LOG_TIME_FILE_NAME);
    }

    @Override
    public boolean writeLog(String log, double totalExecutionTime) throws IOException
    {
        String s = String.valueOf(totalExecutionTime).concat("ms\n");
        return super.writeFile(s.concat(log));
    }

    @Override
    public boolean clearLogFile()
    {
        return super.clearFile(App.LOG_TIME_FILE_NAME);
    }
}
