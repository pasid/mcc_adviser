package org.modcs.mccadv.input;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;

import method.advisor.ClassNode;
import method.japa.parser.ParseException;

import org.apache.commons.io.FileUtils;
import org.modcs.mccadv.spn.AnalysisPerformer;
import org.modcs.mccadv.utils.Constants;
import org.modcs.mccadv.utils.MetricEnum;
import org.modcs.mccadv.utils.PropertiesReader;
import org.modcs.mccadv.utils.StatisticTypeEnum;

import advisor.main.MethodAdvisor;

/**
 * This class is used only for fixed simulation
 * @author airton
 *
 */
public class SimulationManagerFixedConfig{

	
	/**
	 * @param numberOfResources - Refers to the number of resources
	 * 
	 */
	public void run() throws FileNotFoundException, ParseException, IOException, Exception {
		FileUtils.forceMkdir(new File(Constants.AUX_FILES_FOLDER));
		FileUtils.forceMkdir(new File(Constants.RESULTS_FOLDER));
		
		createInputFile();
		
		runAnalysis();
	}

	private synchronized void createInputFile() throws FileNotFoundException, ParseException, IOException {
		FileCreator.createjavaClassIntermediateFormat(PropertiesReader.NUMBER_PARALLEL_JOBS);
		ClassNode cn = MethodAdvisor.runMethodAdvisor(Constants.AUX_FILES_FOLDER+FileSystems.getDefault().getSeparator()+PropertiesReader.NUMBER_PARALLEL_JOBS+"_resources_"+Constants.INPUT_JAVA_FILE_NAME);

		new FileCreator(PropertiesReader.NUMBER_PARALLEL_JOBS, Constants.INPUT_TIME_FILE_NAME, MetricEnum.TIME).createMethodsAndWriteOnFile(cn);
		new FileCreator(PropertiesReader.NUMBER_PARALLEL_JOBS, Constants.INPUT_ENERGY_FILE_NAME, MetricEnum.ENERGY).createMethodsAndWriteOnFile(cn);
		
		System.out.println("File inputTimeFile.txt prepared.");
	}

	private synchronized void runAnalysis() throws FileNotFoundException, ParseException, IOException, Exception {
		ClassNode cn = MethodAdvisor.runMethodAdvisor(Constants.AUX_FILES_FOLDER+FileSystems.getDefault().getSeparator()+PropertiesReader.NUMBER_PARALLEL_JOBS+"_resources_"+Constants.INPUT_JAVA_FILE_NAME);

		double initialTime = System.currentTimeMillis();
		CreateSpn createSPN = new CreateSpn();
		createSPN.createMethods(cn);
		createSPN.readInputFile(PropertiesReader.NUMBER_PARALLEL_JOBS, MetricEnum.TIME);
//		createSPN.readInputFile(PropertiesReader.NUMBER_PARALLEL_JOBS, MetricEnum.ENERGY);
		
//     	AnalysisPerformer.executeAnalysis_to_BarPlot_Fixed(createSPN, StatisticTypeEnum.MTTE);
//		
//		AnalysisPerformer.executeAnalysis_to_BarPlot_Fixed(createSPN, StatisticTypeEnum.THROUGHPUT);
		
        AnalysisPerformer.executeASetOfTransitentSimulation_LinePlot(createSPN, PropertiesReader.NUMBER_PARALLEL_JOBS);

		System.err.println("Simulation for " + PropertiesReader.NUMBER_PARALLEL_JOBS+ " is FINISHED, executed over " + (((System.currentTimeMillis() - initialTime) / 1000) / 60)/60 + " h");
	}
	
	
}
