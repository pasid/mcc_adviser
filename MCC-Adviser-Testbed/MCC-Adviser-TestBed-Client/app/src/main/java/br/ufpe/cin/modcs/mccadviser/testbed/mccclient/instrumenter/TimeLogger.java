package br.ufpe.cin.modcs.mccadviser.testbed.mccclient.instrumenter;

public class TimeLogger extends Logger
{
    private double initTime;

    public void registerStart()
    {
        initTime = System.currentTimeMillis();
    }

    public double registerFinishing()
    {
        return System.currentTimeMillis() - initTime;
    }
}