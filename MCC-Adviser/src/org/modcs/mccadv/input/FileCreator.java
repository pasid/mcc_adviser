/**
 * This class create file to put the times for method calls.
 */

package org.modcs.mccadv.input;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.util.ArrayList;

import org.modcs.mccadv.spn.Method;
import org.modcs.mccadv.utils.Constants;
import org.modcs.mccadv.utils.MetricEnum;
import org.modcs.mccadv.utils.PropertiesReader;

import advisor.main.MethodAdvisor;
import method.advisor.ClassNode;
import method.advisor.Dependence;
import method.advisor.MethodNode;
import method.japa.parser.ParseException;

/**
 * This class create file to put the times for method calls.
 */
public class FileCreator {

	private ArrayList<Method> methodList;
	private File fo;
	private FileWriter fw;
	private BufferedWriter bw;
	private String fileName;
	private MetricEnum metricEnum;

	public FileCreator(int resourcesNumber, String fileName, MetricEnum metricEnum) {
		this.setMetricEnum(metricEnum);
		if (PropertiesReader.FIXED_CONFIGURATION) {
			this.setFileName(Constants.AUX_FILES_FOLDER + FileSystems.getDefault().getSeparator()
					+ resourcesNumber + "_resources_" + fileName);
		}else {
			this.setFileName(fileName);
		}
			
		
		this.methodList = new ArrayList<Method>();
	        
		try {
			if (PropertiesReader.FIXED_CONFIGURATION) {
				this.fo = new java.io.File(Constants.AUX_FILES_FOLDER + FileSystems.getDefault().getSeparator()
						+ resourcesNumber + "_resources_" + fileName);
			} else {
				this.fo = new java.io.File(fileName);
				if (!this.fo.exists()) {
					this.fo.createNewFile();
				}
			}

			this.fw = new FileWriter(fo, false);
			this.bw = new BufferedWriter(fw);


		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Create a method list based on ClassNode object.
	 * 
	 * @param cn Object with Method dependence structure.
	 * @param lastCallDelay
	 * @param parallelCallsDelay
	 * @throws IOException
	 */
	public void createMethodsAndWriteOnFile(ClassNode cn) throws IOException {

		for (MethodNode mn : cn.getMethodNodes()) {
			for (Dependence d : mn.getDependences()) {

				Method method1 = null;
				Method method2 = null;

				if (d.getAssign() != null) {
					method1 = searchMethod(d.getAssign().toString());
					if (method1 == null) {
						method1 = new Method();
						method1.setName(d.getAssign().toString());
						this.methodList.add(method1);

						// Caso queira colocar um tempo fixo.
						this.bw.write(method1.getName() + "=" + PropertiesReader.DELAY_TO_PARALLEL_CALLS);
						this.bw.newLine();

					}
				}

				if (d.getCall() != null) {
					method2 = searchMethod(d.getCall().toString());
					if (method2 == null) {
						method2 = new Method();
						method2.setName(d.getCall().toString());
						if (method1 != null) {
							method2.setDependsOn(method1);
						}
						this.methodList.add(method2);

						if (method2.getName().contains(Constants.NAME_LAST_CALL)) {
							this.bw.write(method2.getName()+"="+ PropertiesReader.DELAY_TO_LAST_CALL);
						} else {
							this.bw.write(method2.getName() +"="+  PropertiesReader.DELAY_TO_PARALLEL_CALLS);
						}
						this.bw.newLine();

					} else {
						if (method1 != null) {
							method2.setDependsOn(method1);
						}

					}
					if (method1 != null) {
						method1.setDependents(method2);
					}
				}
			}
		}
		
		if (this.fileName.equals(Constants.INPUT_TIME_FILE_NAME) && this.getMetricEnum().equals(MetricEnum.TIME)) {
			this.bw.write("totalTime=");
			this.bw.newLine();
		}
		this.bw.flush();
	}
	
	/**
	 * Read and interprets input_methods.java file for construct the inputTimeFile.txt file.
	 * @param cn Object with Method dependence structure.
	 * @throws IOException
	 */
	public void createMethodsAndWriteOnFile() throws IOException{
		
		ClassNode cn = null;
		try {
			cn = MethodAdvisor.runMethodAdvisor("input_methods.java");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(MethodNode mn : cn.getMethodNodes()){
			for(Dependence d : mn.getDependences()){

				Method method1 = null;
				Method method2 = null;

				if(d.getAssign() != null){
					method1 = searchMethod(d.getAssign().toString());
					if(method1 == null){
						method1 = new Method();
						method1.setName(d.getAssign().toString());
						this.methodList.add(method1);

						//Caso queira deixar o campo em branco para o usu�rio digitar.
						this.bw.write(method1.getName() + "=");

						this.bw.newLine();

					}
				}

				if(d.getCall() != null){
					method2 = searchMethod(d.getCall().toString());
					if(method2 == null){
						method2 = new Method();
						method2.setName(d.getCall().toString());
						if(method1 != null){
							method2.setDependsOn(method1);
						}
						this.methodList.add(method2);

						this.bw.write(method2.getName() + "=");

						this.bw.newLine();

					}else{
						if(method1 != null){
							method2.setDependsOn(method1);
						}

					}
					if(method1 != null){
						method1.setDependents(method2);
					}
				}
			}
		}
		
		if (this.fileName.equals(Constants.INPUT_TIME_FILE_NAME) && this.getMetricEnum().equals(MetricEnum.ENERGY)) {
			this.bw.write("totalTime=");
			this.bw.newLine();
		}
		
		closeFileTime();
		
		System.out.println("The file "+ this.fo.getName() +" was created with success.\n");
	}
	

	/**
	 * Close the write stream.
	 * @throws IOException
	 */

	private void closeFileTime() throws IOException {

		bw.close();
		fw.close();

	}
	
	/**
	 * Search for a Method by the method call name.
	 * @param methodName The name of method call.
	 * @return The Method.
	 */

	public Method searchMethod(String methodName) {
		for (int i = 0; i < this.methodList.size(); i++) {
			if (this.methodList.get(i).getName().equals(methodName)) {
				return this.methodList.get(i);
			}
		}
		return null;
	}

	
	/**
	 * Method used to create a java class with a number of X parallel calls (parameter parallelCallsNumber)
	 * @param parallelCallsNumber
	 */
	public static void createjavaClassIntermediateFormat(int parallelCallsNumber) {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(Constants.AUX_FILES_FOLDER+FileSystems.getDefault().getSeparator()+parallelCallsNumber+"_resources_"+Constants.INPUT_JAVA_FILE_NAME);
			writer.write("package mainpack;\n");
			writer.write("public class Test {\n");
			writer.write("    public int rootMethod(){\n");

			for (int i = 0; i < parallelCallsNumber; i++) {
				writer.write("    		String a = add();\n");
			}

			writer.write("    	return addAll(a);\n");
			writer.write("		}\n");
			writer.write("}\n");

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.close();
	}
	
	public static void createFolder(String folderName) {
		File f = new java.io.File(folderName);
		System.out.println("FOlder " + folderName);
		if (!f.exists()) {
			if (f.mkdirs()) {
				System.out.println("The folder " + folderName + " was created successfully.\n");
			}else{
				System.out.println("The folder " + folderName + " was not created, probably because there is no permission. Try to change the folder at config.properties/GENERATED_FILES_PATH.\n");
			}
		}else{
			System.out.println("The folder " + folderName + " already exists!");
		}
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public MetricEnum getMetricEnum() {
		return metricEnum;
	}

	public void setMetricEnum(MetricEnum metricEnum) {
		this.metricEnum = metricEnum;
	}
	
	
	

}
