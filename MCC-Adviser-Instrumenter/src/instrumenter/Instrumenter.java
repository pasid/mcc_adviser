package instrumenter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.ValidationException;

import org.apache.commons.io.FileUtils;

import granularity_analyser.component.Zipper;
import granularity_analyser.method.ClassNode;
import granularity_analyser.method.MethodNode;
import granularity_analyser.method.NodeDependence;
import method.japa.parser.JavaParser;
import method.japa.parser.ParseException;
import method.japa.parser.ast.CompilationUnit;
import method.japa.parser.ast.body.MethodDeclaration;
import method.japa.parser.ast.body.VariableDeclarator;
import method.japa.parser.ast.expr.AssignExpr;
import method.japa.parser.ast.expr.Expression;
import method.japa.parser.ast.expr.MethodCallExpr;
import method.japa.parser.ast.expr.VariableDeclarationExpr;
import method.japa.parser.ast.visitor.VoidVisitorAdapter;

/**
 * 
 * @author airton
 *
 */
public class Instrumenter {
	
		private List<ClassNode> classNodes = new ArrayList<ClassNode>();

		
		@SuppressWarnings("resource")
		public static void createNewJavaFile(String content, String fullFilePath) throws IOException{
			//new RandomAccessFile(fullFilePath, "rw");
			File file = new File(fullFilePath);
			
			FileWriter fw = new FileWriter(file);
			fw.write(content);
			fw.flush();
			fw.close();
			fw = null;
			
			System.gc();
		}

		public static String createNewFileName(String name){
			return name.substring(0,name.length()-5)+'2'+name.substring(name.length()-5,name.length());
			
		}
		
		/**
		 * This method instrument a java class file by inserting before and after method-calls, 
		 * methods to capture the execution time of each method-call  
		 * @param dirOrigin
		 * @param className
		 * @param method 
		 * @return 
		 * @throws FileNotFoundException
		 * @throws ParseException
		 * @throws IOException
		 */
		public static String runInstrumenter(String dirDestiny,String dirOrigin, String dirAuxFiles, String methodName) {
			String className = null;
			try {
				className = Utils.searchFile(dirOrigin);
			} catch (ValidationException e1) {
				e1.printStackTrace();
				return e1.getMessage();
			}
			
		
			String resultOperation = "";
			
			try {
				
				resultOperation = instrumentCode(dirOrigin, className, methodName);
				
				copyAuxiliaryFiles(dirOrigin, dirAuxFiles);

				copyFile(dirDestiny, dirOrigin, className);
				Zipper.zipDir(dirDestiny, "ZippedFile.zip", dirOrigin);
				 
//				Utils.deleteFilesOnDir(dirOrigin);
				
			}catch(FileNotFoundException e){
				e.printStackTrace();
				resultOperation = "The target class " +className+ " was not found at directory "+ dirOrigin;
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				resultOperation = "The code does not follow Java sintax rules.";
			} catch (IOException e) {
				e.printStackTrace();
				resultOperation = "Error accessing a file";
			}
			
			return resultOperation;
	}

		private static void copyAuxiliaryFiles(String dirOrigin, String dirAuxFiles) throws IOException {
			copyFile(dirOrigin, dirAuxFiles, "InstrumenterUtils.java");
			copyFile(dirOrigin, dirAuxFiles, "ManageFile.java");
		}

		private static String instrumentCode(String dirOrigin, String className, String methodName)
				throws FileNotFoundException, ParseException, IOException {
			DumpVisitor.TARGET_METHOD_NAME = methodName;
			FileInputStream in = new FileInputStream(dirOrigin + className);
			CompilationUnit cu;
			// parse the file
			cu = JavaParser.parse(in);
			createNewJavaFile(cu.toString(), dirOrigin + className);
			in.close();
			
			return "The code of class "+ className +" was instrumented successfully.";
		}
		
		
		public static boolean testCode(String dirOrigin, String className, String methodName) {
			DumpVisitor.TARGET_METHOD_NAME = methodName;
			try {
				FileInputStream in = new FileInputStream(dirOrigin + className);
				CompilationUnit cu;
				cu = JavaParser.parse(in);
				in.close();
			} catch (Exception e) {
				return false;
			}
			return true;
		}
		
		
		public static void copyFile(String destiny, String origin, String name) throws IOException {
			String content = FileUtils.readFileToString(new File(origin+name), StandardCharsets.UTF_8.name());
			createNewJavaFile(content, destiny + name);
		}

		private static class MethodCallVisitor extends VoidVisitorAdapter {
			@Override
			public void visit(MethodCallExpr n, Object arg) {
					int line = (((MethodNode) arg).getStartLine() + n.getEndLine()) - 1;
					if (n.getArgs() != null && !n.getArgs().isEmpty()) {
						NodeDependence node = new NodeDependence(false, convert(n.getArgs()), n.getName(), line);
						((MethodNode) arg).getCallNodes().add(node);
					}else{
						NodeDependence node = new NodeDependence(false,  new ArrayList<String>(), n.getName(), line);
						((MethodNode) arg).getCallNodes().add(node);
					}
				}
			private List<String> convert(List<Expression> list) {
				List<String> mylist = new ArrayList<String>();
				for (Expression expression : list) {
					mylist.add(expression.toString());
				}
				return mylist;
			}
		}
		
		private static class ExpAssignVisitor extends VoidVisitorAdapter {
			@Override
			public NodeDependence visit(AssignExpr n, Object arg) {
				
				Expression value = n.getValue();
				MethodCallExpr mcexp = null;
				if (value instanceof MethodCallExpr) {
					mcexp = (MethodCallExpr) value;
					
					if (mcexp.getArgs() == null || mcexp.getArgs().isEmpty()) {
						return null;
					}
					int line = (((MethodNode) arg).getStartLine() + n.getEndLine()) - 1;
					NodeDependence node = new NodeDependence(true, Arrays.asList(n.getTarget().toString()), mcexp.getName(), line);
					((MethodNode) arg).getAssignNodes().add(node);
					return node;
				}
				return null;
			}
		}

		private static class VariableDeclarationExprVisitor extends VoidVisitorAdapter {
			@Override
			public NodeDependence visit(VariableDeclarationExpr n, Object arg) {
				int line = (((MethodNode) arg).getStartLine() + n.getEndLine()) - 1;
				if (n.getVars() != null && n.getVars().get(0) instanceof VariableDeclarator) {
					VariableDeclarator vd = (VariableDeclarator) n.getVars().get(0);
					if(vd.getInit() instanceof MethodCallExpr){
						MethodCallExpr mc = (MethodCallExpr) vd.getInit();
						NodeDependence node = new NodeDependence(true, Arrays.asList(vd.getId().toString()), mc.getName(), line);
						((MethodNode) arg).getAssignNodes().add(node);
						return node;
					}
				}
				
				return null;
			}
		}
		
		
		/**
		 * Simple visitor implementation for visiting MethodDeclaration nodes.
		 */
		private static class MethodVisitor extends VoidVisitorAdapter {

			@Override
			public void visit(MethodDeclaration n, Object arg) {
				CompilationUnit cu = null;

				System.out.println("mamaeee");
				
				try {
					String nomeArqTem = "temp.txt";
					File file = new File(nomeArqTem);
					if (!file.exists()) {
						file.createNewFile();
					} else {
						RandomAccessFile arquivo = new RandomAccessFile(
								nomeArqTem, "rw");

						// vamos excluir todo o conte�do do arquivo
						arquivo.setLength(0);

						arquivo.close();
					}
					FileWriter fw = new FileWriter(file);
					fw.write("public class Temp{");
					System.out.println(n.toString());
					fw.write(n.toString());
					fw.write("}");
					fw.flush();

					cu = JavaParser.parse(file);
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				MethodNode methodNode = new MethodNode(n, ((ClassNode) arg).getFileName(), ((ClassNode) arg).getJavaPackage());
				((ClassNode) arg).getMethodNodes().add(methodNode);

				new MethodCallVisitor().visit(cu, methodNode);
				new ExpAssignVisitor().visit(cu, methodNode);
				new VariableDeclarationExprVisitor().visit(cu, methodNode);
				
				// here you can access the attributes of the method.
				// this method will be called for all methods in this
				// CompilationUnit, including inner class methods
			}
		}
	}
