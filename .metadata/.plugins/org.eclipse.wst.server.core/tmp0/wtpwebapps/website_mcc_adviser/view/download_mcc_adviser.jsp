<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Custom favicon -->
<!-- <link rel="icon" href="../../favicon.ico"> -->

<title>Download MCC-Adviser</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="../resources/bootstrap/css/bootstrap.min.css" />

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link rel="stylesheet"
	href="../resources/bootstrap/css/ie10-viewport-bug-workaround.css" />

<!-- Custom styles -->
<link rel="stylesheet" type="text/css" href="../resources/css/style.css" />
</head>
<body>

	<div class="site-wrapper">
		<div class="site-wrapper-inner">
			<div class="cover-container">

				<div class="masthead clearfix">
					<div class="inner">
						<h3 class="masthead-brand">MCC-Adviser</h3>
						<nav>
						<ul class="nav masthead-nav">
							<li class="active"><a href="#" onclick="document.location.href = '../'; return true;">Home</a></li>
						</ul>
						</nav>
					</div>
				</div>

				<div class="inner cover">
					<h1 class="cover-heading">Download MCC-Adviser</h1>
					<p class="lead">Here you can download the MCC-Adviser Desktop Version.<br>Following, we describe how to use it.</p>
					<div class="inner cover2">
						<h1><a href="../download"><font color="blue">Download MCC-Adviser Here</font></a></h1>
						<div class="row" style="text-align:left">
							<br><br>
							<h2 align="left" class="cover-heading">Instructions</h2>
							The MCC-Adviser-Desktop is delivered as a jar application.Therefore, you have to install the Java JRE (>=7) first. After that, just download and decompress the MCC-Adviser.zip. The content is structured as follows:
							<br><br>
							
							MCC-Adviser/<br>
							******** MCC-Adviser-Desktop/<br>
							**************** MCC-Adviser-Desktop.jar  <font color="red">Run this executable to perform the evaluation and generate graphs.</font><br>
							**************** input_methods.java  <font color="red">Insert your class containing the desired method to be evaluated.</font><br>
							**************** inputTimeFile.txt  <font color="red">Insert on this file the execution times per method-call.</font><br>
							**************** inputEnergyFile.txt  <font color="red">Insert on this file the energy consumptions per method-call.</font><br>
							**************** inputTimeFile.txt  <font color="red">Insert on this file the execution times per method-call.</font><br>
							**************** CDFChart.png  <font color="red">Generated CDF Line Plot.</font><br>
							**************** MTTEChart.png  <font color="red">Generated MTTE Bar Plot.</font><br>
							**************** THROUGHPUTChart.png  <font color="red">Generated Throughput Bar Plot.</font><br>
							**************** MCETEChart.png  <font color="red">Generated MCETE Bar Plot (**energy consumption).</font><br>
							******** MCC-Adviser-Instrumenter/<br>
							**************** aux_inst_files/  <font color="red">Auxiliary Java files.</font><br>
							************************ InstrumenterUtils.java  <font color="red">The instrumented class will call registering methods from this class.</font><br>
							************************ ManageFile.java  <font color="red">This class assists the creation of log files inside the mobile device.</font><br>
							**************** destity/  <font color="red">The instrumented class will be generated inside this folder.</font><br>
							************************ InstrClass.zip  <font color="red">This is the generated zip containing the auxiliary java classes and teh instrumented class.</font><br>
							**************** origin/  <font color="red">Put here your class to be instrumented. You may use the original name.</font><br>
							************************ MyClass  <font color="red">Java class example.</font><br>
							****************MCC-Adviser-Instrumenter.jar  <font color="red">Executes this jar to instrument your code.</font><br>
							<br>
							<h2 align="left" class="cover-heading">Running the MCC-Adviser</h2>
							<br>
							<p align="left">Copy your Java class content to input_methods.java and run the MCC-Adviser-Desktop.jar, following the guide.</p>
							<img class="img-responsive" src="../images/print-time-mcc-desktop.gif"/>
							<h2 align="left" class="cover-heading">Running the Instrumenter</h2>
							<p align="left">Put your Java class inside the origin folder and run the MCC-Adviser-Instrumenter.jar.</p>
							<img class="img-responsive" src="../images/print-instrumenter.gif"/>
						</div>
					</div>
				</div>

				<div class="mastfoot">
					<div class="inner">
						<div style="color: #000; text-shadow: none;">
							MCC-Adviser Tool ©2016. Developed by members of<a href="http://www.modcs.org/">MoDCS</a> group.
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Google jQuery API -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

	<!-- Bootstrap core JavaScript -->
	<script src="./resources/bootstrap/js/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="./resources/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>