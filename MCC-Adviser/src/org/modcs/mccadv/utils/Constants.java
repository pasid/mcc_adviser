package org.modcs.mccadv.utils;

import java.nio.file.FileSystems;


public class Constants {

	public static String INICIAL_NAME_FINAL_PLACE = "pFinal_";
	public static final String NAME_LAST_PLACE = "FINAL_PLACE";
	public static final String NAME_LAST_IMEDIATE_TRANSITION = "FINAL";
	public static final String RESOURCE_POOL_ACRON = "ResourcePool";
	public static final String INICIAL_ACRON_SECOND_PLACE = "p2_";
	public static final String INICIAL_ACRON_FIRST_PLACE = "p1_";
	public static final String INICIAL_ACRON_TIMED_TRANSITION = "RE_";
	public static final String INICIAL_ACRON_IMEDIATE_TRANSITION = "RI_";
	public static final String NAME_LAST_CALL = "addAll";
	public static final String INPUT_TIME_FILE_NAME = "inputTimeFile.txt";
	public static final String INPUT_ENERGY_FILE_NAME = "inputEnergyFile.txt";
	public static final String INPUT_JAVA_FILE_NAME = "input_methods.java";
	public static final String PROPERTIES_FILE_NAME = "config.properties";
	public static final String ROOT_FOLDER = System.getProperty("user.dir");
	public static final String FILES_FOLDER = ROOT_FOLDER+FileSystems.getDefault().getSeparator()+"generated_files";
	public static final String AUX_FILES_FOLDER = FILES_FOLDER+FileSystems.getDefault().getSeparator()+"aux_files";
	public static final String RESULTS_FOLDER = FILES_FOLDER+FileSystems.getDefault().getSeparator()+"simulation_results";
	
}
