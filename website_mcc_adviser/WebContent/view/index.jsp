<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Custom favicon -->
<!-- <link rel="icon" href="../../favicon.ico"> -->

<title>MCC-Adviser</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="./resources/bootstrap/css/bootstrap.min.css" />

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link rel="stylesheet"
	href="./resources/bootstrap/css/ie10-viewport-bug-workaround.css" />

<!-- Custom styles -->
<link rel="stylesheet" type="text/css" href="./resources/css/style.css" />
</head>
<body>
	<div class="site-wrapper">
		<div class="site-wrapper-inner">
			<div class="cover-container">

				<div class="masthead clearfix">
					<div class="inner">
						<h3 class="masthead-brand"></h3>
						<nav>
						<ul class="nav masthead-nav">
							<!-- <li><a href="#">Home</a></li>
									<li><a href="./view/features.jsp">Features</a></li>
									<li><a href="./view/contact.jsp">Contact</a></li> -->
						</ul>
						</nav>
					</div>
				</div>

				<div class="inner cover">
					<h1 class="cover-heading">MCC-Adviser Tool</h1>
					<p class="lead">
						(Mobile Cloud Performance Evaluation Using Stochastic Models) <br>
						<a href="http://www.modcs.org/">MoDCS Research Group</a><br>
						Contact: <a href="http://cin.ufpe.br/~faps/">http://cin.ufpe.br/~faps/</a>
					</p>
				</div>

				<div class="inner cover2">
					<h2 align="left" class="cover-heading">Overview</h2>

					<div class="row">
						<div class="col-md-7">
							<p align="justify">
								Mobile Cloud Computing (MCC) helps increasing performance of
								intensive mobile applications by offloading heavy tasks to cloud
								computing infrastructures. The first step in this procedure is
								partitioning the application into small tasks and identifying
								those that are better suited for offloading. The method call
								partitioning strategy splits the code into a set of method calls
								that are offloaded to remote servers. Quite often, many
								applications need to make use of multiple servers for parallel
								processing of intensive computational operations. Predicting the
								behavior of such parallelizable applications is not an easy
								task. Deciding the number of remote servers determines the
								performance of the applications and the costs of the cloud
								usage. On one hand, users are interested in improving the
								performance of their applications, so they would like to use as
								many servers as possible, but on the other hand, they would also
								like to reduce their costs by using fewer cloud resources. <br>
								<br> In this work, we propose a Stochastic Petri Net (SPN)
								modeling strategy to represent method call executions of mobile
								cloud systems. This approach enables a designer to plan and
								optimize MCC environments in which SPNs represent the system
								behavior and estimate the execution time of parallelizable
								applications. Based on our strategy, we built MCC-Adviser, an
								extremely easy--to--use graphical tool, which generates and
								solves SPNs. The tool can be used by an application developer or
								by a company willing to plan and design an MCC environment to
								obtain the following information:<br>
								<br> &rarr;&nbsp;estimated application's energy consumption
								and execution time based on the number of remote server
								instances;<br> &rarr;&nbsp;the number of method calls per
								time unit; and <br>&rarr;&nbsp;the probability of finishing
								the application execution by a specific time.<br>
								<br> Based on the modeling approach we have implemented
								MCC-Adviser, an performance evaluation tool for MCC
								applications. MCC-Adviser is implemented in Java and aims at
								assisting software engineers planning MCC environments. The
								figure on the right illustrates a big-picture of how MCC-Adviser
								works. The first step is to execute a kind of experiment using
								one resource target. The objective is to collect the Input Data
								needed to refine the generated model. Next, MCC-Adviser will
								need the source code (to build the SPN structure), the execution
								times (per method-call) and the Consumed Energy (also per
								method-call). Besides, the total execution time must be
								informed. Using these input parameters MCC-Adviser may generate,
								refine and plot a graphs with distinct useful statics.

							</p>
						</div>
						<div class="col-md-5">
							<img class="img-responsive img-mcc1"
								src="images/mccadv-energy-basic-idea.gif">
						</div>
					</div>


					<h2 align="left" class="cover-heading">Code Instrumentation</h2>
					<br>

					<div align="justify">Aiming to refine the stochastic model,
						input parameters are strictly necessary. As far as the MCC-Adviser
						plays the role of an adviser, it is expected that the software
						engineer executes as minimal as possible tasks to get the
						estimated statistics. The input parameters are basically of two
						types: time and energy. The execution time must be collected at
						the cloud side whereas energy consumption must be registered at
						mobile device side. These distinct environments offer distinct
						challenges. The figure below presents the two steps necessary to
						capture the input parameters through registering measurements in
						log files. First, the user informs to MCC-Adviser an specific java
						class which contains some heavy root method. Then, MCC-Adviser
						instruments the code by adding directives before and after each
						method-call. Afterwards, the user chooses a server machine and a
						mobile device executing the application with the instrumented java
						class. The java class will print into a log file the execution
						time and energy consumption specific for each method-call.</div>

					<br> <img class="img-responsive img-mcc2"
						src='images/instrumentation_figura_deitada.gif' /> <br>


				</div>

				<div class="inner cover3">
					<h1 class="cover-heading">
						If you would like to test the application, <br> please choose
						one of the following options:
					</h1>
				</div>

				<div class="well center-block" style="max-width: 500px">
					<br>
					 

					<!-- 					<button type=button	onclick="document.location.href = './view/codeInstrumentationBKP.jsp'; return true;" class="btn btn-warning btn-lg btn-block">Code Instrumentation</button> -->
					
					
					<button type=button
						onclick="document.location.href = './view/download_mcc_adviser.jsp'; return true;"
						class="btn btn-warning btn-lg btn-block">Download Full Desktop Version
					</button>
					<br>
					
					<button type=button
						onclick="document.location.href = './view/simpletimeevaluation.jsp'; return true;"
						class="btn btn-warning btn-lg btn-block">Online Time Evaluation
					</button>
					<button type=button
						onclick="document.location.href = './view/simpleenergyevaluation.jsp'; return true;"
						class="btn btn-warning btn-lg btn-block">Online Energy	Evaluation
					</button>
					
					<form action="ReadFileInstServlet" method="post" style="margin-top:5px">
				        <input type="submit" value="Online Instrumentation" class="btn btn-warning btn-lg btn-block">
				    </form>
					
					<br>
					<!-- 					<button type=button	onclick="document.location.href = './view/complextimeevaluation.jsp'; return true;" class="btn btn-warning btn-lg btn-block">Complex Time Evaluation</button> -->
					<!-- 					<button type=button	onclick="document.location.href = './view/complexenergyenergyEvaluation.jsp'; return true;"	class="btn btn-warning btn-lg btn-block">Complex Energy Evaluation</button> -->


				</div>

				<div class="mastfoot">
					<div class="inner">
						<div style="color: #000; text-shadow: none;">
							MCC-Adviser Tool ©2016. Developed by members of <a
								href="http://www.modcs.org/">MoDCS</a> group.
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Google jQuery API -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

	<!-- Bootstrap core JavaScript -->
	<script src="./resources/bootstrap/js/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="./resources/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>