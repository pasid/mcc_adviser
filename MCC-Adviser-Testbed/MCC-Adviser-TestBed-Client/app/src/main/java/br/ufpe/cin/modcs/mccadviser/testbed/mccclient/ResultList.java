package br.ufpe.cin.modcs.mccadviser.testbed.mccclient;

import java.util.ArrayList;
import java.util.List;

public class ResultList
{
    private List<Result> results;
    private StringBuilder executionTimeByIteration;

    public ResultList()
    {
        results = new ArrayList<Result>();
        executionTimeByIteration = new StringBuilder();
    }

    public void addResult(Result result)
    {
        results.add(result);
    }

    public void addExecutionTimeOfIteration(int iteration, double executionTime)
    {
        executionTimeByIteration.append("iteration;");
        executionTimeByIteration.append(iteration);
        executionTimeByIteration.append(";executionTime;");
        executionTimeByIteration.append(executionTime);
        executionTimeByIteration.append("\n");
    }

    public String getExecutionsTimeOfIterations()
    {
        return executionTimeByIteration.toString();
    }

    public String getResultsOfTimeExecution()
    {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < results.size(); i++)
        {
            sb.append(i);
            sb.append(";");

            sb.append(results.get(i).getResultsOfTimeExecution());
            sb.append("\n");
        }

        sb.append(getExecutionsTimeOfIterations());

        return sb.toString();
    }

    public String getResultsOfEnergyConsumption()
    {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < results.size(); i++)
        {
            sb.append(i);
            sb.append(";");

            sb.append(results.get(i).getResultsOfEnergyConsumption());
            sb.append("\n");
        }

        return sb.toString();
    }

    public String getResults()
    {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < results.size(); i++)
        {
            sb.append(i);
            sb.append(";");

            sb.append(results.get(i));
            sb.append("\n");
        }

        return sb.toString();
    }


    @Override
    public String toString()
    {
        return getResults();
    }
}
