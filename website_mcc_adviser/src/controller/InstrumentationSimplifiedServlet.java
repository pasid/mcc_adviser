package controller;

import java.io.IOException;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import instrumenter.Instrumenter;
import utils.Utils;

public class InstrumentationSimplifiedServlet extends HttpServlet { 
	  protected void doGet(HttpServletRequest request, 
		      HttpServletResponse response) throws ServletException, IOException 
		  {
		  
			String basedir = Paths.get(".").toAbsolutePath().normalize().toString()+"/";
			String dirOrigin = basedir + "origin/"; 
			String dirDestiny = basedir + "destiny/"; 
			String dirAuxFIles = basedir + "aux_inst_files/";
			String fileName = "Example.java";
			String filenameDestity = dirDestiny + fileName;
			
			
			String parameter = request.getParameter("method");
		
			String content = request.getParameter("textarea");
			Instrumenter.createNewJavaFile(content, dirOrigin + fileName);
			
			String resultOperation = Instrumenter.runInstrumenter(dirDestiny,dirOrigin, dirAuxFIles, parameter);
			
			response.setContentType("text/html");
			request.setAttribute("instrumented", Utils.readTimeFile(filenameDestity));
			request.setAttribute("response", resultOperation);
			request.setAttribute("zipfile", dirDestiny+"ZippedFile.zip");
			request.getRequestDispatcher("/view/instrumentation_instrumented.jsp").forward(request, response);
			
		  }  

}
