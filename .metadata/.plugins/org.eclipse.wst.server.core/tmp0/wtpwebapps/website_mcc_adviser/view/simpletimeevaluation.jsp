<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    
	    <!-- Custom favicon -->
	    <!-- <link rel="icon" href="../../favicon.ico"> -->
	    
	    <title>Time Evaluation</title>
	    
	    <!-- Bootstrap core CSS -->
	    <link rel="stylesheet" href="../resources/bootstrap/css/bootstrap.min.css" />
	
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <link rel="stylesheet" href="../resources/bootstrap/css/ie10-viewport-bug-workaround.css" />
	
	    <!-- Custom styles -->
	    <link rel="stylesheet" type="text/css" href="../resources/css/style.css" />
	</head>
	<body>
	
		<div class="site-wrapper">
			<div class="site-wrapper-inner">
				<div class="cover-container">
					
					<div class="masthead clearfix">
						<div class="inner">
							<h3 class="masthead-brand">MCC-Adviser</h3>
							<nav>
								<ul class="nav masthead-nav">
								<li class="active"><a href="#"
								onclick="document.location.href = '../'; return true;">Home</a></li>
							</ul>
						</nav>
						</div>
					</div>
					
					<div class="inner cover">
						<h1 class="cover-heading">Time Evaluation - Step 01</h1>
						<p class="lead">
						Here you can experiment the time performance evaluation, 
						<br>
						please feel free to try out and change the example if desired.
						<br> # The code must follow the Java syntax rules.
						</p>
						
						<div class="well center-block formalign">
							
							<form action="../createTimeFile" method="post" style="text-align:center; color:#000; text-shadow: none;">
							
								<div class="form-group">
									<textarea name="textarea" rows="15" cols="40" class="input" >
package mainpack;
public class Test {
	private void rootMethod() {
		int a = m1();
		m2(a);
		m3();
	}
}
</textarea>
								</div>
								<div class="form-group">
									<!-- <input type="hidden" name="viewid" value="/view/spnCreation.jsp"> -->
									<input type="submit" value="Insert Method-Call Delays" id="btCreate" class="btn btn-warning"/>
								</div>
							</form>

						</div>
					</div>
					
					<div class="mastfoot">
						<div class="inner">
							<div style="color: #000; text-shadow: none;">
								MCC-Adviser Tool ©2016. Developed by members of <a href="http://www.modcs.org/">MoDCS</a> group.
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
    
	    <!-- Google jQuery API -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	    
	    <!-- Bootstrap core JavaScript -->
	    <script src="./resources/bootstrap/js/bootstrap.min.js"></script>
	    
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <script src="./resources/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
	</body>
</html>