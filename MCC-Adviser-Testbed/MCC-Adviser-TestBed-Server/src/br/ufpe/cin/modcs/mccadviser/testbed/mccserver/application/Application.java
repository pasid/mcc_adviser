package br.ufpe.cin.modcs.mccadviser.testbed.mccserver.application;

public class Application 
{
	public static int m1() 
	{
		return NQueens2.localSolveNQueens(8);
	}
	
	public static int m2()
	{
		return NQueens2.localSolveNQueens(9);
	}	
	
	public static int m3()
	{
		return NQueens2.localSolveNQueens(8);
	}	
	
	public static int m4()
	{
		int m1 = m1();
		int m2 = m2();
		int m3 = m3();
		
		return m1 + m2 + m3;
	}	
	
	public static int m5()
	{
		return NQueens2.localSolveNQueens(9);
	}
	
	public static int m6()
	{
		return NQueens2.localSolveNQueens(8);
	}
	
	public static int m7()
	{
		int m4 = m4();
		int m3 = m3();
		
		return m4 + m3;
	}
	
	public static int m8()
	{
		int m1 = m1();
		int m2 = m2();
		
		return m1 + m2;
	}
	
	public static int m9()
	{
		int m3 = m3();
		int m1 = m1();
		
		return m3 + m1;
	}
	
	public static int m10()
	{
		return NQueens2.localSolveNQueens(9);
	}
}