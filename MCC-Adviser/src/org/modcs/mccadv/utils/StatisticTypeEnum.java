package org.modcs.mccadv.utils;

public enum StatisticTypeEnum {

	THROUGHPUT ("THROUGHPUT"),
	CDF("CDF"),
	MTTE("MTTE"),
	MCETE("MCETE");
	
	private String type;

	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private StatisticTypeEnum(String type) {
		this.type = type;
	}
	
}
