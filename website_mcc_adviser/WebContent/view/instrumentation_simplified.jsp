<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    
	    <!-- Custom favicon -->
	    <!-- <link rel="icon" href="../../favicon.ico"> -->
	    
	    <title>Instrumentation Evaluation</title>
	    
	    <!-- Bootstrap core CSS -->
	    <link rel="stylesheet" href="./resources/bootstrap/css/bootstrap.min.css" />
	
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <link rel="stylesheet" href="./resources/bootstrap/css/ie10-viewport-bug-workaround.css" />
	
	    <!-- Custom styles -->
	    <link rel="stylesheet" type="text/css" href="./resources/css/style.css" />
	</head>
	<body>
	
		<div class="site-wrapper">
			<div class="site-wrapper-inner">
				<div class="cover-container">
					
					<div class="masthead clearfix">
						<div class="inner">
							<h3 class="masthead-brand">MCC-Adviser</h3>
							<nav>
								<ul class="nav masthead-nav">
								<li class="active"><a href="#"
								onclick="document.location.href = './'; return true;">Home</a></li>
							</ul>
						</nav>
						</div>
					</div>
					
					<div class="inner cover">
						<h1 class="cover-heading">Code Instrumentation</h1>
						<p class="lead">
						Here you may experiment the code instrumentation functionality, 
						<br>
						please feel free to try out.
						<br> 
						</p>
						
						<div class="well center-block formalign">
							
						<%-- 	<form style="text-align:center; color:#000; text-shadow: none;">
								<div class="form-group">
									<textarea style="width: 500px; height: 306px;" readonly><%=(String)request.getAttribute("class")%></textarea>
								</div>
							</form> --%>
							
							<!-- Insert method name -->
							<form action="InstrumentationSimplifiedServlet" style="color:#000; text-shadow: none;" class="form-inline">
								<div class="form-group">
									<div>
										<textarea name="textarea" style="width: 500px; height: 306px;" readonly><%=(String)request.getAttribute("class")%></textarea>
									</div>
									<input type="text" id="field" name="method" class="form-control" placeholder="Inform method name">
									<input type="submit" id="field_buttom" value="Instrument code" class="btn btn-warning" disabled="disabled">
									<a href="#" onclick="document.location.href = './'; return true;" class="btn btn-primary">Back to main page</a>
								</div>						
							</form>	
							
						</div>
					</div>
					
					<div class="mastfoot">
						<div class="inner">
							<div style="color: #000; text-shadow: none;">
								MCC-Adviser Tool ©2016. Developed by members of <a href="http://www.modcs.org/">MoDCS</a> group.
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
    
	    <!-- Google jQuery API -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	    
	    <!-- Custom JS -->
	    <script src="./resources/js/ajax.js"></script>
	    
	    <!-- Bootstrap core JavaScript -->
	    <script src="./resources/bootstrap/js/bootstrap.min.js"></script>
	    
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <script src="./resources/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
	</body>
</html>