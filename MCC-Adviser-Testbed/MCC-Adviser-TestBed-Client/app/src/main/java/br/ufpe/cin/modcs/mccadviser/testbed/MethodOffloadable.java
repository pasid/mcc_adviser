package br.ufpe.cin.modcs.mccadviser.testbed;

import java.io.Serializable;

public class MethodOffloadable implements Serializable
{
    private static final long serialVersionUID = 5385497634559477354L;

    private String methodName;
    private Object methodReturn;
    private double executionTime;
    private String unitTime = "ms";
    private boolean error;
    private String errorMessage;

    public MethodOffloadable(String method)
    {
        this.methodName = method;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append(getMethodName());
        sb.append("();");
        sb.append(getExecutionTime());
        sb.append(";");
        sb.append(getUnitTime());
        sb.append(";");
        sb.append(getMethodReturn());
        sb.append(";");
        sb.append(isError());
        sb.append(";");
        sb.append(getErrorMessage() == null ? "" : getErrorMessage());

        return sb.toString();
    }

    public String getResultFormatted()
    {
        StringBuilder sb = new StringBuilder();

        sb.append(getMethodName());
        sb.append("()   ");
        sb.append(getExecutionTime());
        sb.append("");
        sb.append(getUnitTime());
        sb.append("  ");
        sb.append(getMethodReturn());
        sb.append("  ");
        sb.append(isError());
        sb.append("  ");
        sb.append(getErrorMessage() == null ? "" : getErrorMessage());

        return sb.toString();
    }

    public String getMethodName()
    {
        return methodName;
    }

    public Object getMethodReturn()
    {
        return methodReturn;
    }

    public void setMethodReturn(Object methodReturn)
    {
        this.methodReturn = methodReturn;
    }

    public double getExecutionTime()
    {
        return executionTime;
    }

    public void setExecutionTime(double executionTime)
    {
        this.executionTime = executionTime;
    }


    public String getUnitTime()
    {
        return unitTime;
    }

    public void setUnitTime(String unitTime)
    {
        this.unitTime = unitTime;
    }

    public boolean isError()
    {
        return error;
    }

    public void setError(boolean error)
    {
        this.error = error;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }
}