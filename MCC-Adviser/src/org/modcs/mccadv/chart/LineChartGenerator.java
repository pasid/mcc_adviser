package org.modcs.mccadv.chart;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.modcs.mccadv.spn.ResultTime;
import org.modcs.mccadv.utils.PropertiesReader;

/**
 * This class create a .png image with a line chart representing Transient Analysis or Simulation.
 *
 */
public class LineChartGenerator {

	private JFreeChart chart;
	private  List<ArrayList<ResultTime>> resultsTransient;;
	private XYSeries [] series;
	private XYSeriesCollection dataset;
	private String fileName;

	public LineChartGenerator(List<ArrayList<ResultTime>> resultsTransient, String fileName){
		this.dataset = new XYSeriesCollection();
		this.resultsTransient = resultsTransient;
		this.series = new XYSeries [resultsTransient.size()];
		this.fileName = fileName;
		
	}
	
	/**
	 * Creates the Transient Analysis chart.
	 */
	public void makeStationaryChart(){
		this.chart = ChartFactory.createXYLineChart("Cumulative Distribution Function", "Execution Time", "Probability of Absortion", dataset, PlotOrientation.VERTICAL, true, true, false);
		
		addDataToChart();
		exportChart(this.fileName);
	}
	//Adiciona os dados no dataset
	public void addDataToChart (){

		
		
		if (PropertiesReader.FIXED_CONFIGURATION) {
				for (int i = 0; i < PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER.length; i++) {
					if (PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER[i] == 1) 
						this.series[i] = new XYSeries(PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER[i] + " Resource");
					else
						this.series[i] = new XYSeries(PropertiesReader.ARRAY_RESOURCE_POOL_NUMBER[i] + " Resources");
					for(int j = 0; j< resultsTransient.get(i).size(); j++){
						this.series[i].add(this.resultsTransient.get(i).get(j).getTime(), this.resultsTransient.get(i).get(j).getProb());
					}

					dataset.addSeries(series[i]);
				}
		}else {
			for(int i = 0; i<this.series.length; i++){
				
				if(i == 0){
					this.series[i] = new XYSeries(1 + "Resource");
				}else{
					this.series[i] = new XYSeries(i+1 + "Resources");
				}
				
				for(int j = 0; j< resultsTransient.get(i).size(); j++){
					this.series[i].add(this.resultsTransient.get(i).get(j).getTime(), this.resultsTransient.get(i).get(j).getProb());
				}
	
				dataset.addSeries(series[i]);
			}
		}	

	}

	/**
	 * Generate the image with the chart.
	 * @param fileName The name of file.
	 */
	public void exportChart(String fileName){

		XYPlot xyPlot = (XYPlot) this.chart.getPlot();
		xyPlot.setDomainCrosshairVisible(true);
		xyPlot.setRangeCrosshairVisible(true);

		NumberAxis range = (NumberAxis) xyPlot.getRangeAxis();
		range.setTickUnit(new NumberTickUnit(0.05));

		try {
			OutputStream image = new FileOutputStream(fileName + ".png");
			ChartUtilities.writeChartAsPNG(image, this.chart, 500, 500);
			image.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
