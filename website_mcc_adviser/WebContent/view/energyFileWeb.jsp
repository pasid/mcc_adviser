<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Custom favicon -->
<!-- <link rel="icon" href="../../favicon.ico"> -->

<title>Energy Evaluation</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css" />

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link rel="stylesheet"
	href="resources/bootstrap/css/ie10-viewport-bug-workaround.css" />

<!-- Custom styles -->
<link rel="stylesheet" type="text/css" href="resources/css/style.css" />
</head>
<body>

	<div class="site-wrapper">
		<div class="site-wrapper-inner">
			<div class="cover-container">

				<div class="masthead clearfix">
					<div class="inner">
						<h3 class="masthead-brand">MCC-Adviser</h3>
						<nav>
						<ul class="nav masthead-nav">
							<li class="active"><a href="#"
								onclick="document.location.href = './'; return true;">Home</a></li>
						</ul>
						</nav>
					</div>
				</div>

				<div class="inner cover" align="center">
					<h1 class="cover-heading">Energy Evaluation - Step 02</h1>
					<p class="lead">
						Now you must inform the execution time and energy consumption for
						each method-call. <br> Fill the information using only
						numbers. <br> You may use float numbers if you want.
					</p>
					<div class="well center-block formalign"
						style="text-align: center; color: #000; text-shadow: none;">

						<form action="saveEnergyFile?action=savefile" method="post">
							<div class="form-group">

								<table style="width: 100%">
									<tr>
										<th><p align="center">Time:</p></th>
										<th><p align="center">Energy:</p></th>
									</tr>
									<tr>
										<td><textarea name="textareatime" rows="15" cols="40"
												class="input" style="color: #000">${requestScope.textAreaTimeContent}</textarea></td>

										<td><textarea name="textareaenergy" rows="15" cols="40"
												class="input" style="color: #000">${requestScope.textAreaEnergyContent}</textarea></td>

									</tr>
								</table>

							</div>
							<div class="form-group">
								<input type="hidden" name="viewid"
									value="/view/energyFileWeb.jsp"> <input type="submit"
									value="Calculate and Generate Chart" id="btPutTime"
									class="btn btn-warning" /> <a href="#"
									onclick="document.location.href = 'view/simpleenergyevaluation.jsp'; return true;"
									class="btn btn-primary">Back to step 01</a>
							</div>
						</form>

						<div id="divCheckbox"
							style="display: none; color: #000; text-shadow: none;">
							<b> Calculating... </b>
						</div>

						<!-- Aqui são os gráficos -->

						<%-- ${sessionScope.explanationCharts} --%>
						<%-- ${sessionScope.imgBarChart} 
							${sessionScope.stationaryExplanation} --%>
						${sessionScope.mceteExplanation}
						${sessionScope.imgMCETEChart} 
						<%-- ${sessionScope.imgLineChart}
							${sessionScope.transientExplanation}
							${sessionScope.probabilityForm}
							${sessionScope.stringProb} --%>
					</div>
				</div>



				<div class="mastfoot">
					<div class="inner">
						<div style="color: #000; text-shadow: none;">
							MCC-Adviser Tool ©2016. Developed by members of <a
								href="http://www.modcs.org/">MoDCS</a> group.
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<script language="javascript">
		$("#btPutTime").on("click", function() {
			$("#divCheckbox").show();
		});
	</script>
	<!-- Google jQuery API -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

	<!-- Bootstrap core JavaScript -->
	<script src="./resources/bootstrap/js/bootstrap.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="./resources/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>